/************************************************************************/
/*******************             DESLAD.H             *******************/
/************************************************************************/

#define _N  0
#define _L  1
#define _A  2
#define _F  3
#define _P  4
#define _PP 5
#define _PC 6
#define _PR 7
#define _T  8
#define _S  9
#define _PPR 10
#define _B  11
#define _J 12
#define _BPR 13
#define _BPPR  14

#define DLIGA 6

#define MAXCTTX 11
#define MAXCTTY 15

extern int cll[MAXCTTX*2][MAXCTTY*2];
extern int ill[MAXCTTX*2][MAXCTTY*2];      /* matriz de estados internos - ladder */
extern int ell[MAXCTTX*2][MAXCTTY*2];

extern int xth,yth,xtr,ytr;
extern char menu_saidas[];

extern void LimpaLadder(int ini,char f_g);
extern void DesenhaLinha(int x,int y);
extern void DesenhaCAberto(int x,int y,int xd,int yd);
extern void DesenhaCFechado(int x,int y,int xd,int yd);
extern void DesenhaBobina(int x,int y,int xd,int yd);
extern void DesenhaSaidas(int x,int y,int xd,int yd,int cord);
extern void DesenhaTimer(int x,int y,int xd,int yd,int cord,char *flagt);
extern void DesenhaBloco(int x,int y,int xd,int yd,int cord);
extern char MontaLadder(int cl,int *ydl);
extern void DesenhaLadder(void);
extern void DesenhaCelula(int xd,int yd,int corf,int cord);
extern char *PesqLabel(int nl,char iendr);
extern void LimpaEndr(void);
extern void LimpaNumLin(void);
extern void DeslBuffer(int endr, int nb);
extern void AjustEndrLinhas(int nl,int nb);
extern void DesenhaQuadro(int xi,int yi,int xf,int yf,void *pima);
extern void DesQuadrMens(int xi,int yi,int xf,int yf,char *s);
extern void MensErroTimer(void);
extern void LeNome(int xi,int yi,int xf,int yf,unsigned int nmax,char *s);
extern void LeInterno(int xi,int yi,int xf,int yf,int *opa,unsigned int vmax,unsigned int nmax,char fhex,char *s);
extern void AcionaTeclaMouse(int xi,int yi,int xf,int yf,int *opc);
extern int  MenuGraf(int nopc,char *sopc);
extern void LimpaDescrInter(char *fn);
extern char EscrDescrInter(int nint);
extern int PesqFimProg(int endr);
extern void DeletaJump(int endr);
extern void ProcInicBlk(int *xd,int *yd);
extern char ProcCNT(int *xd,int yd,int *endr);
extern void VerifTipoSaida(int ns,int endr,char *f_ei,int nl);
extern char ProcuraOperando(int *contl,int *p_oper,char f_again);
extern void EntraDescrInter(int op);
extern void EntraDescrTimr(int op,int *contl);
extern void EntraDescrLabl(int op);
