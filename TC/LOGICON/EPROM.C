#include <sisvar.h>
#define PORT_OUT     0x378
#define PORT_IN      0x379
#define PORT_STR     0x04
#define CLK_ON       0x02
#define CLK_OFF      0x00
#define PORT_IN_DAT  0x20
#define PORT_OUT_DAT 0x01
#define PORT_DAT     0x00

int impr_out,impr_in;
char bufepr[4];

void PutData(void)
  {
   register int cbit,cdad = 0,
                     ch,bb;
   for (;cdad < 4;)
     {
      ch = bufepr[cdad++];
      for (cbit = 1; (cbit != 0x0100); (cbit <<= 1))
        {
         bb = CLK_ON;
         if (ch & cbit)
           bb |= PORT_OUT_DAT;
         Out(impr_out,bb);
         bb ^= CLK_ON;
         Out(impr_out,bb);
        }
     }
   Out(impr_out,PORT_STR);
   Out(impr_out,0);
  }

char GetData(void)
  {
   register int  cbit,
                 ch = 0;

   for (cbit = 0x0001; (cbit != 0x100) ; cbit <<= 1)
     {
      if (In(impr_in) & PORT_IN_DAT)
        ch |= cbit;
      Out(impr_out,CLK_ON);
      Out(impr_out,CLK_OFF);
     }
   return(ch);
  }

