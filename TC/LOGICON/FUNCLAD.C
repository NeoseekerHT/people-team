
#include <alloc.h>
#include <graphics.h>
#include "sisvar.h"
#include "video_io.h"
#include "edbuf.h"
#include "cv.h"
#include "mouse.h"
#include "compil.h"
#include "deslad.h"


const char menu_registros[] =
    { " #   R   &R RLDR ->  +   -   /   *  INCRDECR >=  <=  =  LDX OUTXSBLKLBLK" };

const char opc_registros[] =
    { IMED,REG,INDX,_RLDR,_OUTR,_SUMR,_SUBR,_MULR,_DIVR,_INCR,_DECR,_GT,_LT,_EQ,_LDX,_OUTX,_SBLK,_LBLK,_LDR,'\0' };
                                                                                             /* OBS. : _LDR no fim do vetor */



/* retorna true se o comando registro for com operando constante */

char VerifCmdCte(int *endr)
  {
   register int opc = *(buffer+(*endr)-2) & 0x3F;
   switch ( opc & 0xFC )
     {
      case _LDR  :
      case _SUMR :
      case _SUBR : if ( opc & 0x03 == IMED )
                     {
                      *endr -= 2;
                      break;
                     }
                   return(false);
      default    : switch ( opc & 0xFE )
                     {
                      case _LT   :
                      case _GT   :
                      case _EQ   :
                      case _DIVR :
                      case _MULR : if ( (opc & 0x01) == IMED )
                                     {
                                      *endr -= 2;
                                      break;
                                     }
                      default    : if ( opc == _JUMP )
                                     {
                                      *endr -= 2;
                                      break;
                                     }
                                  return(false);
                     }
                   break;
     }
   return(true);
  }




int near ProcCmd(char *s,char car,int pi)
  {
   register int cc = pi;
   while( *(s+cc) )
     {
      if ( car == *(s+cc) )
        return(cc);
      cc++;
     }
   return(-1);
  }


void near AjustMatBloco(int ncmd,int opc,int ope,char *opc_r,int caso)
  {
   register int ac = ProcCmd(opc_registros,(char)opc,3),
                cc = ncmd*14,bac;
   bac = ac;
   if ( *(opc_registros+ac) == _LDR )
     Copia(opc_r+cc,"    ");
     else
     {
      ac <<= 2;
      Copia(opc_r+cc,Extrai(menu_registros,ac,ac+3));
     }
   Copia(palavra,"  ");
   switch ( *(opc_registros+bac) )
     {
      case _LBLK :
      case _SBLK :
                   break;
      default    : switch ( caso )
                     {
                      case IMED : Concat(palavra,StrW(ope));
                                  break;
                      case INDX : Concat(palavra,"&");
                      case REG  : Concat(palavra,"R");
                                  Concat(palavra,DecHexW(ope)+1);
                                  break;
                     }
     }
   Concat(opc_r+cc,palavra);
  }


int near MontaCmdBloco(int xd,int yd,int *ope_r,int *endr_r,char *opc_r,char *fcte)
  {
   register int noper,cc = 0,ac;
   int nc,cbuf = ell[xd][yd];
   noper = *(buffer+cbuf);
   cbuf += 2;
   *endr_r = cbuf;
   while ( noper )
     {
      *(fcte+cc) = false;
      *(endr_r+cc) = cbuf;
      oper   = buffer[cbuf++];
      opcod  = buffer[cbuf++];
      noper -= 2;
      nc = 0;
      switch ( opcod & 0x3F )
        {
         case _RLDR :
         case _SBLK :
         case _LBLK : *(fcte+cc) = true;
                      break;
         default    : switch ( opcod & 0x3C )
                        {
                         case _LDR  :
                         case _SUMR :
                         case _SUBR : switch ( opcod & 0x03 )
                                        {
                                         case IMED : oper = (int)(buffer[cbuf]) + (int)(buffer[cbuf+1] << 8 );
                                                     cbuf += 2;
                                                     noper -= 2;
                                                     *(fcte+cc) = true;
                                                     break;
                                         case INDX : nc++;
                                         case REG  : nc++;
                                                     oper += (int)((opcod & 0xC0) << 2);
                                                     break;
                                        }
                                      opcod &= 0x3C;
                                      break;
                         default    : switch ( opcod & 0x3E )
                                        {
                                         case _MULR :
                                         case _DIVR :
                                         case _GT   :
                                         case _LT   :
                                         case _EQ   : switch ( opcod & 0x01 )
                                                        {
                                                         case IMED : oper = (int)(buffer[cbuf]) + (int)(buffer[cbuf+1] << 8);
                                                                     cbuf  += 2;
                                                                     noper -= 2;
                                                                     *(fcte+cc) = true;
                                                                     break;
                                                         case REG  : oper += (int)((opcod & 0xC0) << 2);
                                                                     nc = REG;
                                                                     break;
                                                        }
                                                      break;
                                         case _LDX  :
                                         case _OUTX :
                                         case _OUTR :
                                         case _INCR :
                                         case _DECR : nc = (opcod & 0x01) + 1;
                                                      oper += (int)((opcod & 0xC0) << 2);
                                                      break;
                                        }
                                      opcod &= 0x3E;
                                      break;
                        }
         }
      AjustMatBloco(cc,opcod,oper,opc_r,nc);
      if ( !*(fcte+cc) )
        *(ope_r+cc) = oper | 0x400;
      cc++;
     }/*while*/
   *(endr_r+cc) = cbuf;
   return(cc);
  }



void near DesenhaOpcAltB(int xi,int yi,int xf,char *s,char cort,char corf)
  {
   setcolor(cort);
   setfillstyle(SOLID_FILL,corf);
   bar(xi+7,yi+6,xf-7,yi+15);
   outtextxy(xi+10,yi+7,s);
  }


void AlteraBloco(int xd,int yd,int contl)
  {
   const int xi = 180,
             yi = 90,
             xf = 287,
             yf = 305;

   #define MAXOPC 10

   const char menu_altblk[] = { "InsRDelR" };
   const char opc_altblk[] = {_ins,_del};

   register int cc,ac,acc;
   int nopc,copc = 0,
       ebuf = ell[xd][yd],
       cbuf = ebuf + 2;
   void *pima;
   char f_monta = true,
        f_ins,f_n = false,
        f_cte[MAXOPC],
        opc_reg[MAXOPC*14],
        stropc[12];
   int  ope_reg[MAXOPC],endr_reg[MAXOPC];

   pima = malloc(imagesize(xi,yi,xf,yf));
   contl++;
   do
     {
      if ( f_monta )
        {
         DesenhaQuadro(xi,yi,xf,yf,pima);
         nopc = MontaCmdBloco(xd,yd,ope_reg,endr_reg,opc_reg,f_cte);
         setcolor(azul);
         for ( cc = 0 ; cc < nopc ; cc++ )
           {
            ac = 97 + 10 * cc;
            outtextxy(xi+10,ac,opc_reg+cc*14);
           }
         f_monta = false;
        }
      if ( copc < nopc )
        Copia(stropc,opc_reg+copc*14);
        else
        *stropc = '\0';
     LimpaDescrInter(&f_n);
     if ( !*(f_cte+copc) )
       f_n = EscrDescrInter(*(ope_reg+copc));
      cc = copc*10;
      DesenhaOpcAltB(xi,yi+cc,xf,stropc,amarelo,azul);
      AcionaTeclaMouse(xi+7,yi+6,xf-7,yi+15+10*nopc,&copc);
      DesenhaOpcAltB(xi,yi+cc,xf,stropc,azul,cinza);
      if ( mousepc && (tecpc == _enter) )
        {
         cbuf = *(endr_reg+copc);
         if ( copc < nopc )
           Copia(stropc,opc_reg+14*copc);
           else
           *stropc = '\0';
         DesligaMouse();
         DesenhaOpcAltB(xi,yi+copc*10,xf,stropc,amarelo,azul);
         PosMouse(xmouse,ymouse);
         LigaMouse();
         LimpaDescrInter(&f_n);
         if ( !*(f_cte+copc) )
           f_n = EscrDescrInter(*(ope_reg+copc));
         acc = MenuGraf(2,menu_altblk);
         if ( tecpc == _enter )
           {
            tecpc = *(opc_altblk+acc);
            tecfunc = true;
           }
           else
           tecpc = 0;
        }
        else
        if ( mouseok && (tecpc != _esc) && MouseDentro(200,0,500,19) )
          tecpc = 'D';
      if ( tecfunc )
        {
         if ( f_n )
           LimpaDescrInter(&f_n);
         switch ( tecpc )
           {
            case _up    : if ( copc )
                            cbuf = *(endr_reg+(--copc));
                          break;
            case _down  : if ( copc < nopc )
                            cbuf = *(endr_reg+(++copc));
                          break;
            case _del   : if ( copc < nopc )
                            {
                             switch ( *(buffer+cbuf+1) & 0x3F )
                               {
                                case _RLDR :
                                case _SBLK :
                                case _LBLK : ac = -2;
                                             break;
                                default    : ac = ( *(f_cte+copc) ) ? -4 : -2;
                                             break;
                               }
                             DeslBuffer(cbuf,ac);
                             AjustEndrLinhas(contl,ac);
                             (*(buffer+ebuf)) += ac;
                             *(f_cte+copc) = true;
                             f_monta = true;
                            }
                          break;
            case _ins   : cc = MenuGraf(18,menu_registros);
                          if ( tecpc == _esc )
                            break;
                          f_ins = false;
                          opcod = *(opc_registros+cc);
                          switch ( cc )
                            {
                             case 0  :
                             case 1  :
                             case 2  : opcod = _LDR | cc;
                                       f_ins = true;
                                       break;
                             case 3  : if ( *(buffer+cbuf+1) == _RLDR )
                                         {
                                          (*(buffer+cbuf))++;
                                          f_monta = true;
                                          break;
                                         }
                                       cc = IMED;
                                       opcod = _RLDR;
                                       f_ins = true;
                                       break;
                             case 16 :  /* LBLK/SBLK*/
                             case 17 : cc = NDAT;
                                       f_ins = true;
                                       break;
                             default :
                                       do
                                         {
                                          cc = MenuGraf(3,menu_registros);
                                          if ( tecpc == _esc )
                                            break;
                                          switch ( opcod )
                                            {
                                             case _LDR  :
                                             case _SUMR :
                                             case _SUBR : f_ins = true;
                                                          break;
                                             case _MULR :
                                             case _DIVR :
                                             case _LT   :
                                             case _GT   :
                                             case _EQ   : f_ins = ( cc != INDX ) ? true : false;
                                                          break;
                                             case _LDX  :
                                             case _OUTX :
                                             case _OUTR :
                                             case _INCR :
                                             case _DECR : f_ins = ( cc != IMED ) ? true : false;
                                                          break;
                                            }
                                         }while ( !f_ins && (tecpc != _esc) );
                                       break;
                            }
                          if ( f_ins )
                            {
                             switch ( cc )
                               {
                                case IMED : if ( opcod == _RLDR )
                                               {
                                                do
                                                  {
                                                   oper = 0;
                                                   LeInterno(250,150,328,190,&oper,10,2,false,"Val.:");
                                                  }while ( !oper );
                                               }
                                               else
                                               {
                                                oper = 0 ;
                                                LeInterno(250,150,352,190,&oper,(unsigned int)65535,5,false,"Val.:");
                                                ac = 4;
                                                DeslBuffer(cbuf,ac);
                                                *(buffer+cbuf) = 00;
                                                *(buffer+cbuf+1) = (char)opcod;
                                                *(buffer+cbuf+2) = (char)oper;
                                                *(buffer+cbuf+3) = (char)(oper >> 8);
                                                f_ins = false;
                                               }
                                             opcod |= cc;
                                             break;
                                case INDX  :
                                case REG   : oper = 0;
                                             LeInterno(250,150,335,190,&oper,MAXINTERNO,3,true,"REG.:");
                                             switch ( opcod )
                                               {
                                                case _LDX  :
                                                case _OUTX :
                                                case _OUTR :
                                                case _INCR :
                                                case _DECR : cc--;
                                                             f_ins = false;
                                                             break;
                                               }
                                             opcod |= (char)((oper & 0x0300) >> 2);
                                             opcod |= cc;
                                             if ( !f_ins )
                                               {
                                                cc++;
                                                f_ins = true;
                                               }
                                             break;
                               }
                             if ( f_ins )
                               {
                                ac = 2;
                                DeslBuffer(cbuf,ac);
                                *(buffer+cbuf)   = (char)oper;
                                *(buffer+cbuf+1) = (char)opcod;
                                *palavra = '\0';
                                acc = (int)(oper | (opcod & 0xC0) << 2) | 0x400;
                                if ( (cc == REG) || (cc == INDX) || !*(texto+acc) )
                                  EntraDescrInter(acc);
                               }
                             (*(buffer+ebuf)) += ac;
                             AjustEndrLinhas(contl,ac);
                             f_monta = true;
                             tecpc = 0;
                            }
                          break;
           }
        }
        else
        switch ( toupper(tecpc) )
          {
           case 'D' : if ( !*(f_cte+copc) )
                        {
                         EntraDescrInter(*(ope_reg+copc));
                         LimpaDescrInter(&f_n);
                        }
                      break;
          }
     }while ( (tecpc != _esc) && (tecpc != _enter) );
   if ( !nopc )
     {
      DeslBuffer(ebuf,-2);
      AjustEndrLinhas(contl,-2);
     }
   MontaReferencia();
   free(pima);
   tecpc = 0;
   LimpaDescrInter(&f_n);
  }


char AlteraTimer(int contl)
  {
   const int xi = 150,
             yi = 130,
             xf = 248,
             yf = 178;

   register int cc;
   register int ebuf = ell[xth][yth];
   int ntim = (int)*(buffer+ebuf),
       btim = ntim,
       copc = 0,
       ac;
   char stim[4][11];
   void *pima;

   pima = malloc(imagesize(xi,yi,xf,yf));
   DesenhaQuadro(xi,yi,xf,yf,pima);
   for ( cc = 0 ; cc < 4 ; cc++ )
     stim[cc][0] = '\0';
   do
     {
      if ( !*(timr+ntim) )
        AlocaDatTimr(ntim,contl,0xFF,"");
      setcolor(cinza);
      for ( cc = 0 ; cc < 4 ; cc++ )
        outtextxy(xi+7,yi+cc*10+5,*(stim+cc));
      Copia(*stim," TIMER #");
      Concat(*stim,DecHex((char)ntim));
      if ( (*timr[ntim]).codt.bit.up )
        Copia(*(stim+1),"    UP");
        else
        Copia(*(stim+1),"   DOWN");
      if ( (*timr[ntim]).codt.bit.dec )
        Copia(*(stim+2),"    0.1");
        else
        Copia(*(stim+2),"   0.01");
      if ( (*timr[ntim]).codt.bit.pre )
        Copia(*(stim+3),"PRESETAVEL");
        else
        {
         Copia(*(stim+3),"  #");
         cc = TAB_AUTO_LOAD + ntim*2;
         Concat(*(stim+3),StrW((int)*(buffer+cc)+256*(*(buffer+cc+1))));
        }
      setcolor(vermelho);
      for ( cc = 0 ; cc < 4 ; cc++ )
        outtextxy(xi+7,yi+cc*10+5,*(stim+cc));
      do
        {
         AcionaTeclaMouse(xi+7,yi+6,xf-7,yi+55,&copc);
        }while( (tecpc != _enter) && (tecpc != _esc) );
      if ( tecpc == _esc )
        break;
      switch ( copc )
        {
         case 0 : do
                    {
                     ac = 0;
                     LeInterno(200,150,278,190,&ac,(int)cfg_ntim-1,2,true,"Temp:");
                     if ( *(timr+ac) && (ac != ntim) && (tecpc == _enter) )
                       DesQuadrMens(150,180,350,210,"TEMPORIZADOR EXISTENTE !");
                    }while( (tecpc != _enter) && (tecpc != _esc) );
                  if ( (ac != ntim) && (tecpc == _enter) )
                    {
                     free(*(timr+ntim));
                     *(timr+ntim) = NULL;
                     ntim = ac;
                     *(buffer+ebuf) = (char)ntim;
                    }
                  tecpc = 0;
                  break;
         case 1 : (*timr[ntim]).codt.bit.up ^= 0x01;
                  break;
         case 2 : (*timr[ntim]).codt.bit.dec ^= 0x01;
                  break;
         case 3 : (*timr[ntim]).codt.bit.pre ^= 0x01;
                  ac = 0xFFFF;
                  if ( !(*timr[ntim]).codt.bit.pre )
                    LeInterno(250,150,352,190,&ac,(unsigned int)65535,5,false,"Val.:");
                  cc = TAB_AUTO_LOAD + ntim*2;
                  *(buffer+cc) = (char)ac;
                  *(buffer+cc+1) = (char)(ac >> 8);
                  break;
        }
     }while( tecpc != _esc );
   LimpaEndr();
   putimage(xi,yi,pima,COPY_PUT);
   free(pima);
   tecpc = 0;
   if ( btim - ntim )
     return(true);
   return(false);
  }

void near PesqContSaida(axd,ayd,auxf,auxf1)

  int axd,ayd;
  char *auxf,*auxf1;
  {
   register int ax = axd + 1;
   *auxf = *auxf1 = true;
   while ( *auxf && *auxf1 )
     {
      switch ( cll[ax][ayd] )
        {
         case _B   :
         case _J   :
         case _S   : *auxf1 = false;
                     break;
         case _PR  :
         case _P   : switch ( cll[ax][ayd+1] )
                       {
                        case _PR :
                        case _P  : if ( ell[axd+1][ayd] )
                                     *auxf = false;
                                   ax++;
                                   break;
                        default  : *auxf = false;   /* condicao erro */
                                   break;
                       }
                     break;
         case _A   :
         case _F   :
         case _L   :
         case _PPR :
         case _PP  : if ( ell[axd+1][ayd] )
                       *auxf = false;
                     ax++;
                     break;
         default   : *auxf = false;
                     break;
        }
     }
  }

int PesqEndrRLD(int axd,int ayd)
  {
   register int ax = axd + 1;
   register int ay = ( (ayd == ytr) || !ayd ) ? ayd : ayd - 2;
   char auxf = false;
   int opc,endr;

   while ( !auxf )
     {
      switch ( cll[ax][ay] )
        {
         case _N : ay -= 2;
                   break;
         case _B :
         case _T :
         case _A :
         case _F : auxf = true;
                   break;
         case _L : ax += 2;
                   while ( !auxf )
                     {
                      switch ( cll[ax][ay] )
                        {
                         case _L : ax += 2;
                                   break;
                         default : auxf = true;
                                   break;
                        }
                     }
                   break;
        }
     }
   endr = ell[ax][ay] + 1;
   do
     {
      endr -= 2;
      if ( *(buffer+endr) == _RLD )
        auxf = VerifCmdCte(&endr);
     }while( auxf );
   return(endr);
  }

void near InsereSaidaDiagr(int *contl,int ns)
  {
   register int cc;
   register int ebuf = ( lin_prog ) ? (*linha[*contl]).endr : 0;
   char auxf;
   ebuf = ( *contl+1 == lin_prog ) ? PesqFimProg(ebuf) : (*linha[*contl+1]).endr;
   DeslBuffer(ebuf,2);
   AjustEndrLinhas(*contl+1,2);
   VerifTipoSaida(ns,ebuf,&auxf,*contl);
   linha[lin_prog] = (struct rlinha *) malloc(sizeof(struct rlinha));
   if ( !*(linha+lin_prog) )
     MensErr(MALOC);
   cc = lin_prog++;
   while ( cc > *contl+1 )
     {
      (*linha[cc]).endr = (*linha[cc-1]).endr;
      cc--;
     }
   (*linha[++(*contl)]).endr = ebuf;
  }



char InsereSaida(xd,yd,contl,ebuf,f_ei)
  int xd,yd,*contl,*ebuf;
  char *f_ei;
  {
   register int cc,ac,ebufa;
   char auxf = false,
        auxf1 = false;
   int axd,ayd,nld;

   LimpaEndr();
   switch ( cll[xd][yd] )
     {
      case _N  :
                 break;
      case _S  :
      case _J  : xd--;
      case _L  : axd = xd + 1;
                 ayd = yd;
                 while ( cll[axd][ayd] == _L )
                   axd++;
                 switch ( cll[xd+1][yd] )
                   {
                    case _J :
                    case _S : xd++;
                              break;
                   }
                 switch ( cll[axd][ayd] )
                   {
                    case _A :
                    case _F : PesqContSaida(axd,ayd,&auxf,&auxf1);
                              if ( auxf )
                                {
                                 ac = MenuGraf(15,menu_saidas);
                                 auxf = ( tecpc == _esc ) ? false : true;
                                 if ( auxf && (ac >= 11) )
                                   {
                                    InsereSaidaDiagr(contl,ac);
                                    auxf = false;
                                   }
                                }
                              break;
                    case _B :
                    case _S :
                    case _J : ac = MenuGraf(15,menu_saidas);
                              if ( tecpc == _esc )
                                break;
                              if ( ac >= 11 )
                                {
                                 InsereSaidaDiagr(contl,ac);
                                 return(true);
                                }
                                else
                                {
                                 *ebuf = ell[axd][ayd];
                                 if ( *(buffer+*ebuf+1) == _JUMP )
                                   switch ( *(buffer+*ebuf) )
                                     {
                                      case _END  :
                                      case _JMP  :
                                      case _CALL :
                                      case _RET  : auxf = false;
                                                   break;
                                      default    : auxf = true;
                                     }
                                }
                              cc = xd;
                              while ( !auxf )
                                {
                                 cc--;
                                 switch( cll[cc][ayd] )
                                   {
                                    case _BPPR:
                                    case _P   :
                                    case _PPR :
                                    case _PR  : if ( cll[cc][ayd-1] != _N )
                                                  auxf =  true;
                                    case _L   :
                                    case _PP  :
                                                break;
                                    default   : auxf = true;
                                                break;
                                   }
                                }
                              *ebuf = ell[axd][ayd] + 1;
                              ebufa = *ebuf - 2;
                              switch ( cll[cc][ayd] )
                                {
                                 case _T :
                                 case _A :
                                 case _F : if ( *(buffer+ebufa) == _RLD )
                                             {
                                              auxf1 = true;
                                              break;
                                             }
                                 case _B : ebufa++;
                                           DeslBuffer(ebufa,2);
                                           AjustEndrLinhas(*contl+1,2);
                                           *(buffer+ebufa) = 01;
                                           *(buffer+ebufa+1) = _RLD;
                                           *ebuf += 2;
                                           break;
                                 default : ebufa = PesqEndrRLD(cc,ayd);
                                           (*(buffer+ebufa-1))++;
                                           break;
                                }
                              break;
                   }
                 if ( auxf )
                   {
                    if ( auxf1 )
                      {
                       int acc = *ebuf;
                       while ( auxf1 )
                         {
                          acc -= 2;
                          if ( (*(buffer+acc) == _RLD) && !VerifCmdCte(&acc) )
                            auxf1 = false;
                         }
                       (*(buffer+acc-1))++;
                      }
                      else
                      auxf1 = false;
                    do
                      {
                       opcod = *(buffer+*ebuf) & 0x3F;
                       switch( opcod )
                         {
                          case _BLK : *ebuf += (*(buffer+*ebuf-1) + 2);
                                      break;
                          case _JUMP: switch ( *(buffer+*ebuf-1) )
                                        {
                                         case _JMP  :
                                         case _CALL :
                                         case _JPC  :
                                         case _CALC : *ebuf += 2;
                                                      break;
                                        }
                                      auxf1 = true;
                                      break;
                          default   : if ( Dentro((char)opcod,saidas) )
                                        auxf1 = true;
                                        else
                                        *ebuf += 2;
                                      break;
                         }
                      }while( !auxf1 );
                   }
                 break;
      case _A  :
      case _F  : PesqContSaida(xd,yd,&auxf,&auxf1);
                 if ( !auxf )
                   break;
                 ac = MenuGraf(15,menu_saidas);
                 if ( (tecpc == _esc) )
                   {
                    auxf = false;
                    break;
                   }
                 if ( ac >= 11 )
                   {
                    auxf = false;
                    InsereSaidaDiagr(contl,ac);
                    return(true);
                   }
                 *ebuf = ell[xd][yd] + 3;
                 opcod = *(buffer+*ebuf) & 0x3F;
                 switch( opcod )
                   {
                    case _RLD : nld = ++(*(buffer+*ebuf-1));
                                do
                                  {
                                   *ebuf += 2;
                                   opcod = *(buffer+*ebuf) & 0x3F;
                                   if ( Dentro((char)opcod,saidas) )
                                     nld--;
                                  }while( nld );
                                break;
                    default   : DeslBuffer(*ebuf-1,2);
                                AjustEndrLinhas(*contl+1,2);
                                *(buffer+*ebuf) = _RLD;
                                *(buffer+*ebuf-1) = 01;
                                do
                                  {
                                   *ebuf += 2;
                                   opcod = *(buffer+*ebuf) & 0x3F;
                                   if ( opcod == _JUMP )
                                     {
                                      *ebuf += 2;
                                      break;
                                     }
                                  }while( !Dentro((char)opcod,saidas) );
                                break;
                   }
                 break;
     }
   if ( auxf )
     {
      (*ebuf)++;
      DeslBuffer(*ebuf,2);
      AjustEndrLinhas(*contl+1,2);
      VerifTipoSaida(ac,*ebuf,f_ei,*contl);
      return(true);
     }
   return(false);
  }

void InsereLinhaDiagr(int *contl)
  {
   register int ebuf = ( lin_prog ) ? (*linha[*contl]).endr : 0;
   register int cc;

   ebuf = ( (*contl+1 == lin_prog) || !lin_prog ) ? PesqFimProg(ebuf) : (*linha[*contl+1]).endr;
   DeslBuffer(ebuf,4);
   AjustEndrLinhas(*contl+1,4);
   linha[lin_prog] = (struct rlinha *) malloc(sizeof(struct rlinha));
   if ( !*(linha+lin_prog) )
     MensErr(MALOC);
   cc = lin_prog++;
   while ( cc > *contl+1 )
     {
      (*linha[cc]).endr = (*linha[cc-1]).endr;
      cc--;
     }
   (*linha[cc]).endr = ebuf;
   moper = 0;
   *(buffer+ebuf)   = oper  = 0xFF;
   *(buffer+ebuf+1) = opcod = _LD;
   AjustaReferencia(cc);
   *(buffer+ebuf+2) = 0xFF;
   *(buffer+ebuf+3) = opcod = _OUT;
   AjustaReferencia(cc);
   *contl = cc;
  }


char InsereTimerDiagr(int *contl)
  {
   register int cc;
   register int ebuf = ( lin_prog ) ? (*linha[*contl]).endr : 0;
   int ntim = 0;
   LeInterno(200,150,278,190,&ntim,(int)cfg_ntim-1,2,true,"TIMR:");
   if ( tecpc == _esc )
     return(false);
   if ( *(timr+ntim) )
     {
      MensErroTimer();
      return(false);
     }
   ebuf = ( *contl+1 == lin_prog ) ? PesqFimProg(ebuf) : (*linha[*contl+1]).endr;;
   DeslBuffer(ebuf,10);
   AjustEndrLinhas(*contl+1,10);
   linha[lin_prog] = (struct rlinha *) malloc(sizeof(struct rlinha));
   if ( !*(linha+lin_prog) )
     MensErr(MALOC);
   cc = lin_prog++;
   while ( cc > *contl+1 )
     {
      (*linha[cc]).endr = (*linha[cc-1]).endr;
      cc--;
     }
   (*linha[++(*contl)]).endr = ebuf;
   *palavra = '\0';
   EntraDescrTimr(ntim,contl);
   moper = 0;
   *(buffer+ebuf++) = oper  = 0xFF;
   *(buffer+ebuf++) = opcod = _LD;
   AjustaReferencia(*contl+1);
   AjustaReferencia(*contl+1);
   *(buffer+ebuf++) = 0xFF;
   *(buffer+ebuf++) = _LD;
   *(buffer+ebuf++) = (char)ntim;
   *(buffer+ebuf++) = _TIMR;
   *(buffer+ebuf++) = 0xFF;
   *(buffer+ebuf++) = opcod = _OUT;
   *(buffer+ebuf++) = 0xFF;
   *(buffer+ebuf++) = _OUT;
   AjustaReferencia(*contl+1);
   AjustaReferencia(*contl+1);
   return(true);
  }

void DeletaLinhaDiagr(int *contl,int yini,int yfim)
  {
   register int cc;
   int yd,nbytes,endri,endrf;
   if ( !lin_prog )
     return;
   for ( yd = yini ; yd < yfim ; yd += 2 )
     {
      if ( cll[MAXCTTX*2-2][yd] == _J )
        switch ( cll[MAXCTTX*2-1][yd] )
          {
           case _RET  :
           case _RETC :
           case _END  : break;
           default    : DeletaJump(ell[MAXCTTX*2-2][yd]);
                        break;
          }
     }
   endri = (*linha[*contl]).endr;
   endrf = ( *contl+1 == lin_prog ) ? PesqFimProg(endri) : (*linha[*contl+1]).endr;
   if ( *PesqLabel(endri,true) )
     {
      free(*(nlabel+oper));
      *(nlabel+oper) = NULL;
     }
   if ( xth )  /* existe timer */
     {
      cc = (int)*(buffer+ell[xth][yth]);
      free(timr[cc]);
      timr[cc] = NULL;
     }
   nbytes = endrf-endri;
   DeslBuffer(endri,-nbytes);
   lin_prog--;
   for ( cc = *contl ; cc < lin_prog ; cc++ )
     {
      (*linha[cc]).endr = (*linha[cc+1]).endr;
      (*linha[cc]).endr -= nbytes;
     }
   free(linha[lin_prog]);
   linha[lin_prog] = NULL;
   MontaReferencia();
   if ( (*contl == lin_prog) && *contl )
     (*contl)--;
  }

void AtualizaCfg(void)
  {
   register int cc = 0;
   *(buffer+NUM_ED)  = cfg_ed;
   *(buffer+NUM_SD)  = cfg_sd;
   *(buffer+NUM_PLC) = cfg_nclp;
   *(buffer+NUM_TIMER)   = cfg_ntim;
   *(buffer+REG_TIMER)   = (char)cfg_rtim;
   *(buffer+REG_TIMER+1) = (char)(cfg_rtim >> 8);
   *(buffer+NUM_BLK)   = cfg_nblk;
   *(buffer+REG_BLK)   = (char)cfg_rblk;
   *(buffer+REG_BLK+1) = (char)(cfg_rblk >> 8);
   for ( ; cc < cfg_ntim ; cc++ )
     *(buffer+TAB_CFG_TIMER+cc) = (*timr[cc]).codt.byte;
  }
