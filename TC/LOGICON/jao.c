/***************************** COMPLOG teste ******************************/

#include <stdio.h>
#include <stdlib.h>

#include <dos.h>
#include <alloc.h>
#include "sisvar.h"
#include "cv.h"
#include "video_io.h"
#include "edbuf.h"
#include "menus.h"
#include "edit.h"
#include "funclad.h"
#include "deslad.h"
#include "ladder.h"
#include "compil.h"
#include "com.h"

#define COM0 0x3f8
#define COM1 0x2f8
#define TAXA 9600L
#define PARIDADE 0
#define STOP 1
#define BITS_DADOS 8
#define TRUE 1

#define     linsup "�����������������������������������������������������������ͻ"
#define     linesp "�  Nome  � Mem. �             Ocorrencias                   �"
#define     linmed "�����������������������������������������������������������Ķ"
#define     linvaz "�        �      �                                           �"
#define     lininf "�����������������������������������������������������������ͼ"
#define     LF 10
#define     CR 13
#define     FF 12
#define     lin_impr1 "+----------+------+"
#define     lin_impr2 "---------------------------------------------------------------------------------------------+"
#define     lin_impr3 "|   NOME   | MEM. |              REFERENCIA      CRUZADA                                                        |"
#define     lin_impr4 "|          |      | "
#define     lin_sim   "��������������������������ENTRADAS������������������������Ĵ"


#define MENSINIC "*** C O M P L O G I ***         ver 3.02        L O G I C O N  Automacao"
#define path "C:"


  char
     sair,
     sopc[6],sopr[6],
     jan_ativ,
     acion_sim,
     esc_arq,
     esc_edit,
     esc_princ,
     esc_comp,
     esc_com,
     c_monit_ei,
     lab_monit_ei[16][9];

/*** Simulacao ***/
   char
       work,
       est_int[MAXINTERNO+1],
       pilha_bit[16],
       c_pilha_endr,c_pilha_bit,c_pilha_reg,
       fim_var;
   int
       reg_int[MAXINTERNO+1],
       pilha_endr[8],
       pilha_reg[16],
       cont_varr,timerdec,timercent;

 struct codb
  {
   unsigned  int up :1;
   unsigned  int dec:1;
   unsigned  int r  :1;
   unsigned  int h  :1;
   unsigned  int b  :1;
   unsigned  int a  :1;
   unsigned  int d  :1;
   unsigned  int pre:1;
  };

 struct tipo_temp
  {
   unsigned int pr,ef;
   union
     {
      char cod;
      struct codb bit;
     }codt;
  };


   struct tipo_temp  temp[MAXTIMR];
   int cont_temp,offsd,offed;
   char
       monit_temp[9],
       buffed[64];

const  char af[2] = { 'A','F' };
const  char lig = 2;
const  char des = 1;

/******** Prototipos ********/

void Transmitir(void);
void Receber(void);
void Envia_Arq(char *);
void Recebe_Arq(char *);

char near *NomeArq(char *s)
  {
   register int cc = 0;
   while ((*s) && (*s != '.'))
     {
      *(bs + cc++) = *s;
      s++;
     }
   *(bs+cc) = '\0';
   return(bs);
  }

char near *ExtArq(char *s)
  {
   register int cc = 0;

   while ((*s) && (*s != '.'))
     s++;
   if (*s == '.')
     {
      s++;
      while (*s)
        {
         *(bs + cc++) = *s;
         s++;
        }
     }
   *(bs + cc) = '\0';
   return (bs);
  }

void near InicDadosTexto(void)
  {
   y_txt = x_txt = 2;
   lin_txt = pag_txt = 0;
  }


void near MoldRefer(void)
  {
   register int cc;
   CorFundo(cinza);
   CorTexto(azul);
   EscrXY(12,3,linsup);
   EscrXY(12,4,linesp);
   EscrXY(12,5,linmed);
   for (cc = 6;cc < 23; cc++)
     EscrXY(12,cc,linvaz);
   EscrXY(12,cc,lininf);
  }

char near ProcDef(char *s,int i)
  {
   register int cc = 0;
   while( (cc < cont_def) && ((*def[cc]).inter != i) )
     cc++;
   if ( cc == cont_def )
     {
      Copia(s,"  ****  ");
      return(false);
     }
   Copia(s,ComplVazio((*def[cc]).nome,8));
   return(true);
  }

char near ProcDescr(char *s,int i)
  {
   if ( *(texto+i) )
     {
      Copia(s,*(texto+i));
      return(true);
     }
   Copia(s,"  ---  SEM DESCRICAO  --- ");
   return(false);
  }


void near ListaRefer(int pp)
  {
   register int cy = 6,ca,cc,cr = pp;
   char inter[5],nome[9],cx;
   while( (cy < 23) && (cr <= 2*MAXINTERNO+1) )
     {
      ca = ProcInter(cr);
      if ( ca < 0 )
        {
         cr++;       /* proxima tentativa */
         continue;   /* nao encontrou referencia */
        }
      *inter = VerifEISR(cr);
      *(inter+1) = '\0';
      Concat(inter,Extrai(DecHexW(cr & 0xFBFF),1,3));
      ProcDef(nome,cr);
      EscrXY(13,cy,nome);
      EscrXY(23,cy,inter);
      cx = 30;
      for (cc = 0; cc < (*refx[ca]).cntx ;cc++)
        {
         Copia(nome,StrW(abs((*refx[ca]).linx[cc])));
         if ((*refx[ca]).linx[cc] < 0)
           {
            char cca = 4;
            while( cca && (nome[cca] != ' ') )
              cca--;
            nome[cca] = '@';
           }
         EscrXY(cx,cy,nome);
         cx += 5;
         if ( cx > 66 )
           {
            if (cy >= 22)
              break;
            cx = 30;
            cy++;
            EscrXY(13,cy,"        �     ");
           }
        }
      while ( cx < 66 )
        {
         EscrXY(cx,cy,"     ");
         cx += 5;
        }
      cy++;
      cr++;
     }
   for (;(cy < 23);cy++)
     {
      EscrXY(13,cy,"        �     ");
      EscrXY(30,cy,ComplVazio(" ",40));
     }
  }

void near ReferCruzada(void)
  {
   register int ptst,pag = 0;
   MoldRefer();
   ListaRefer(pag);
   do
     {
      LeTecla();
      switch (tecpc)
        {
         case _down : ptst = pag + 1;
                      while( (ptst <= 2*MAXINTERNO+1) && (ProcInter(ptst) < 0) )
                        ptst++;
                      if ( ptst <= 2*MAXINTERNO+1 )
                        {
                         pag = ptst;
                         ListaRefer(pag);
                        }
                      break;
         case _up   : ptst = pag - 1;
                      while( (ptst >= 0) && (ProcInter(ptst) < 0) )
                        ptst--;
                      if ( ptst >= 0 )
                        {
                         pag = ptst;
                         ListaRefer(pag);
                        }
                      break;
        }
     }while( tecpc != _esc );
   LimpaQuadro(12,3,60,22);
   tecpc = 0;
  }

void VerifLinhaVazia(int lin)
  {
   if ( !(char)(PosFimSpc(texto[lin])+1) )
     {
      free(texto[lin]);
      texto[lin] = (char *) malloc (2);
      if ( !*(texto+lin) )
        MensErr(MALOC);
      Copia(texto[lin],";");
     }
  }


void near InicializaHeap(void)
  {
   register int cc;
   for (cc = 0; cc < MAXLINTXT;cc++)
     texto[cc] = NULL;
   for (cc = 0; cc < MAXLINPRG;cc++)
     linha[cc] = NULL;
   for ( cc = 0 ; cc < MAXTIMR ; cc++ )
     timr[cc] = NULL;
   for (cc = 0; cc < MAXDEF;cc++)
     {
      refx[cc] = NULL;
      def[cc] = NULL;
     }
   for (cc = 0; cc < MAXLABEL;cc++)
     {
      nlabel[cc] = NULL;
      elabel[cc] = NULL;
     }
   for (cc = 0; cc < MAXJUMP;cc++)
     jump[cc] = NULL;
  }



void near VerifArqCFG(void)
  {
   FILE *fg;
   char adir[30];

   if ( !(fg = fopen("CL.CFG","rb")) )
     {
      Copia(nome_arq,"SEM_NOME.LST");
      Quadro(25,12,31,2,"  Arquivo LP.CFG inexistente!  Utilizando configuracao padrao.");
      Copia(dado_cfg.nome,nome_arq);
      if ( !(fg = fopen("CL.CFG","wb")) )
        {
         CorFundo(preto);
         LimpaTela();
         EscrXY(1,1,"ERRO NO DRIVE...");
         exit(1);
        }
      fwrite(&dado_cfg,sizeof(struct tipo_dado_cfg),1,fg);
      fclose(fg);
      LeTecla();
      LimpaQuadro(25,12,31,2);
     }
     else
     {
      fread(&dado_cfg,sizeof(struct tipo_dado_cfg),1,fg);
      Copia(nome_arq,dado_cfg.nome);
      fclose(fg);
     }
  }

void near SalvarArqCFG(void)
  {
   FILE *fg;

   Copia(dado_cfg.nome,nome_arq);
   if ( !(fg = fopen("CL.CFG","wb")) )
     {
      CorFundo(preto);
      LimpaTela();
      EscrXY(1,1,"ERRO NO DRIVE...");
      exit(1);
     }
   fwrite(&dado_cfg,sizeof(struct tipo_dado_cfg),1,fg);
   fclose(fg);
  }

void near VerifArqSim(void)
  {
   FILE *fpr;
   register int cc,ccc;
   Copia(bufftxt,NomeArq(nome_arq));
   Concat(bufftxt,".");
   Concat(bufftxt,"SIM");
   
   if ( !(fpr = fopen(bufftxt,"rb")) )
     return;
   fread(&c_monit_ei,sizeof(c_monit_ei),1,fpr);
   fread(&monit_ei,2,c_monit_ei,fpr);
   for ( cc = 0 ; cc < c_monit_ei ; cc++ )
     {
      ccc = 0;
      while( ((*def[ccc]).inter != monit_ei[cc]) && (ccc < cont_def) )
        ccc++;
      if ( ccc < cont_def )
        Copia(lab_monit_ei[cc],(*def[ccc]).nome);
        else
        {
         *bufftxt = VerifEISR(monit_ei[cc]);
         *(bufftxt+1) = '\0';
         Concat(bufftxt,Extrai(DecHexW(monit_ei[cc]),1,3));
         Copia(lab_monit_ei[cc],bufftxt);
        }
     }
   fread(&cont_temp,sizeof(cont_temp),1,fpr);
   fread(&monit_temp,1,cont_temp,fpr);
   for ( cc = 0 ; cc < cont_temp ; cc++ )
     fread(&temp[monit_temp[cc]].pr,2,1,fpr);
   fclose(fpr);
  }

void near SalvarArqSim(void)
  {
   FILE *fpr;
   register int cc;
   Copia(bufftxt,NomeArq(nome_arq));
   Concat(bufftxt,".");
   Concat(bufftxt,"SIM");
   
   if ( !(fpr = fopen(bufftxt,"wb")) )
     {
      CorFundo(preto);
      LimpaTela();
      EscrXY(1,1,"ERRO NO DRIVE...");
      exit(1);
     }
   fwrite(&c_monit_ei,sizeof(c_monit_ei),1,fpr);
   fwrite(&monit_ei,2,c_monit_ei,fpr);
   fwrite(&cont_temp,sizeof(cont_temp),1,fpr);
   fwrite(&monit_temp,1,cont_temp,fpr);
   for ( cc = 0 ; cc < cont_temp ; cc++ )
      fwrite(&temp[monit_temp[cc]].pr,2,1,fpr);
   fclose(fpr);
  }

void near VerifArqDat(void)
  {
   FILE *fpr;
   register int cc;
   char cp;
   struct rlabel auxlabl;
   struct rtimr  auxtimr;
   Copia(bufftxt,NomeArq(nome_arq));
   Concat(bufftxt,".");
   Concat(bufftxt,"DAT");
   if ( !(fpr = fopen(bufftxt,"rb")) )
     {
      fclose(fpr);
      return;
     }
   for ( cc = 0 ; cc < MAXLABEL ; cc++ )
     {
      fread(&auxlabl,sizeof(struct rlabel),1,fpr);
      if ( auxlabl.endr >= 0 )
        {
         *(nlabel+cc) = (struct rlabel *) malloc(sizeof(struct rlabel));
         if ( !*(nlabel+cc) )
           MensErr(MALOC);
         Copia((*nlabel[cc]).nome,auxlabl.nome);
         (*nlabel[cc]).endr = auxlabl.endr;
        }
     }
   for ( cc = 0 ; cc < MAXTIMR ; cc++ )
     {
      fread(&auxtimr,sizeof(struct rtimr),1,fpr);
      if ( auxtimr.codt.byte )
        AlocaDatTimr(cc,-1,auxtimr.codt.byte,auxtimr.nome);
     }
   for ( cc = 0 ; cc < MAXLINTXT ; cc++ )
     {
      fread(&cp,1,1,fpr);
      if ( cp )
        {
         fread(&palavra,cp,1,fpr);
         *(palavra+cp) = '\0';
         AlocaDatDescr(cc,palavra);
        }
     }
   fclose(fpr);
  }


void near SalvarArqDat(void)
  {
   const char vaz16[] = { "                " };
   const char vaz30[] = { "                              " };
   FILE *fpr;
   register int cc;
   int cp;
   struct rlabel auxlabl;
   struct rtimr  auxtimr;

   Copia(auxlabl.nome,vaz16);
   auxlabl.endr = -1;
   Copia(auxtimr.nome,vaz30);
   auxtimr.codt.byte = 0x00;
   Copia(bufftxt,NomeArq(nome_arq));
   Concat(bufftxt,".");
   Concat(bufftxt,"DAT");
   if ( !(fpr = fopen(bufftxt,"wb")) )
     {
      CorFundo(preto);
      LimpaTela();
      EscrXY(1,1,"ERRO NO DRIVE...");
      exit(1);
     }
   for ( cc = 0 ; cc < MAXLABEL ; cc++ )
     if ( *(nlabel+cc) )
       fwrite(*(nlabel+cc),sizeof(struct rlabel),1,fpr);
       else
       fwrite(&auxlabl,sizeof(struct rlabel),1,fpr);
   for ( cc = 0 ; cc < MAXTIMR ; cc++ )
     if ( *(timr+cc) )
       fwrite(*(timr+cc),sizeof(struct rtimr),1,fpr);
       else
       fwrite(&auxtimr,sizeof(struct rtimr),1,fpr);
   for ( cc = 0 ; cc < MAXLINTXT ; cc++ )
     if ( *(texto+cc) && flag_ladder )
       {
        cp = ComprStr(*(texto+cc));
        fwrite(&cp,1,1,fpr);
        fwrite(*(texto+cc),cp,1,fpr);
       }
       else
       {
        cp = 0;
        fwrite(&cp,1,1,fpr);
       }
   fclose(fpr);
  }


void near Inicializacao(void)
  {
   Copia(strp," ");
   jan_ativ = sair = false;
   esc_princ = esc_arq = esc_edit = esc_comp = esc_com =0;
   fim_lin = -1;
   InicDadosTexto();
   flag_ladder = flag_i = true;
   inic_buffer = 0;
   fim_buffer = MAXBUFF;
   flag_compilado = false;
   c_monit_ei = cont_temp = 0;
   offsd = offed = 0;
   ZeraVariaveis();
  }

void near MenuPrinc(void){
   const char num_opc = 6;
   register char cc;
   char opc[6][12] =
     {
      "  Arquivo ",
      "  Editar  ",
      "Compilacao",
      "  Buffer  ",
      " Simulacao",
      "  Serial  "
     };
   char aux,aux_jan,
        sai_menu = false;

   CorTexto(azul);
   CorFundo(cinza);
   EscrXY(1,1,ComplVazio(" ",80));
   for (cc = 0;cc < num_opc;cc++)
     EscrXY(cc*12+1,1,opc[cc]);
   do{
      aux = esc_princ*12+1;
	  CorFundo(verde);
      CorTexto(marrom);
      EscrXY(aux,1,opc[esc_princ]);

      if (jan_ativ && (esc_princ < 6) )
      {
		aux_jan = aux + 2;
		switch ( esc_princ )
		   {
		    case 0 : Janela(aux_jan,2,9,4,"Ler      Gravar   ImpressaoSair     ","LGIS",&esc_arq);
                break;
            case 1 : Janela(aux_jan,2,8,2,"ProgramaTelas   ","PT",&esc_edit);
                break;
            case 2 : Janela(aux_jan,2,10,3,"Compilar  Ref.Cruz. LADDER   ","CRL",&esc_comp);
				break;
			case 5 : Janela(aux_jan,2,10,2,"Envia Prg Recebe Prg","ER",&esc_com);
				break;
           }
      }
      else
      LeTecla();
	  if (tecpc != _enter){
         CorFundo(cinza);
         CorTexto(azul);
         EscrXY(aux,1,opc[esc_princ]);
		 
		 if (jan_ativ){
            switch (esc_princ)
              {
               case 0 : LimpaQuadro(aux_jan,2,9,4);
                    break;
               case 1 : LimpaQuadro(aux_jan,2,8,2);
                    break;
               case 2 : LimpaQuadro(aux_jan,2,10,3);
					break;
				case 5 : LimpaQuadro(aux_jan,2,10,2);
					break;
			}
		 }
		 switch ( tecpc ){
			case _right  : if ( esc_princ == num_opc-1 )
				esc_princ = 0;
			    else
			    esc_princ++;
			    break;
	 		case _left   : if (esc_princ == 0)
				esc_princ = num_opc-1;
			    else
		 	    esc_princ--;
			    break;
			case _esc    : if ( jan_ativ )
				jan_ativ = false;
				break;
		 }
	  }else {
		  if (jan_ativ)
			  sai_menu = true;
		  else {
			  sai_menu = ((esc_princ == 3) || (esc_princ == 4)) ? true : false;
			  jan_ativ = true;
		  }
	  }
   } while(!sai_menu);

   tecpc = 0;
}

void ListExt(lext,ofs,cm)
  char lext[40][14];
  unsigned int ofs,
               cm;
  {
   register int x = 26,
                y = 0,
                c = 0,
                a;
   MoldQuadro(23,9,30,10);
   do
     {
      a = c + ofs;
      y++;
      EscrXY(x,9+y,lext[a]);
      if (c == 9)
        {
         x = 41;
         y = 0;
        }
      c++;
     } while ((a < cm) && (c < 20));
  }


char EscolheExt(char *ext)
  {
   char narq[40];
   register char  ax = 26,
                  ay = 10,
                  cc = 0,
                  ac;
   unsigned int err,
            cext = 0,
            offset = 0;
   struct   ffblk lista;
   char     arq_ext[40][14];

   Copia(narq,path);
   Concat(narq,"*.");
   Concat(narq,ext);
   err =  findfirst(narq,&lista,0);
   while ((!err) && (cext < 40))
     {
      Copia(arq_ext[cext++],lista.ff_name);
      err = findnext(&lista);
     }
   if (!cext)
     {
      Copia(narq,"Nenhum arquivo *.");
      Concat(narq,Maiuscula(ext));
      Quadro(10,9,20,1,narq);
      LeTecla();
      LimpaQuadro(10,9,20,1);
      return(true);
     }
   cext--;
   ListExt(arq_ext,offset,cext);
   do
     {
      ac = cc + offset;
      CorTexto(marrom);
      CorFundo(verde);
      EscrXY(ax,ay,arq_ext[ac]);
      LeTecla();
      CorTexto(azul);
      CorFundo(cinza);
      EscrXY(ax,ay,arq_ext[ac]);
      if (tecfunc)
        {
         switch (tecpc)
           {
            case _up   : if (cc)
                           {
                            if (cc == 10)
                              {
                               ax = 26;
                               ay = 19;
                              }
                              else
                              ay--;
                            cc--;
                           }
                         break;
            case _down : if ((cc < 19) && (ac < cext))
                           {
                            if (cc == 9)
                              {
                               ax = 41;
                               ay = 10;
                              }
                              else
                              ay++;
                            cc++;
                           }
                         break;
            case _right: if ((cc < 10) && (ac + 9 < cext))
                           {
                            cc += 10;
                            ax = 41;
                            ay = cc;
                           }
                         break;
            case _left : if (cc > 9)
                           {
                            cc -= 10;
                            ax = 26;
                           }
                         break;
            case _pgup : if (offset > 0)
                           {
                            offset -= 20;
                            ListExt(arq_ext,offset,cext);
                            cc = 0;
                            ax = 26;
                            ay = 10;
                           }
                         break;
            case _pgdown: if (cext >= (offset + 20))
                            {
                             offset += 20;
                             ListExt(arq_ext,offset,cext);
                             cc = 0;
                             ax = 26;
                             ay = 10;
                            }
                         break;
           }
        }
     } while ((tecpc != _enter) && (tecpc != _esc));
   if (tecpc == _esc)
     {
      LimpaQuadro(23,9,30,10);
      return(true);
     }
   Copia(nome_arq,arq_ext[cc]);
   LimpaQuadro(23,9,30,10);
   return(false);
  }

unsigned int near LerArqNorm(char *na,int inic,int fim)
  {
   char buf[CBYTES];
   char narq[40];
   register int cb = 0,
                nl = CBYTES,
                cc;
   FILE  *fp;

   Copia(narq,path);
   Concat(narq,na);

   if ( !(fp = fopen(narq,"rb")) )
     return(cb);
   cont_buf = inic;
   tecpc = 0;
   while ((tecpc != _esc) && (nl == CBYTES) && (cont_buf <= fim))
     {
      if (TeclaPress())
        LeTecla();
      nl = fread(buf,1,CBYTES,fp);
      for (cc = 0; cc < nl;cc++)
        buffer[cont_buf+cc] = buf[cc];
      cont_buf += nl;
      cb += nl;
     }
  fclose(fp);
  return (cb);
 }


void near LerArq(void)
  {
   int cont_lin = 0;
   char na[40],av[40],ext[4],
        si[5],sf[5],ll,
        fim_arq = false,
        arq_log = false,
        arq_lad = false;
   FILE *fp;

   Copia(av,nome_arq);
   Quadro(5,4,31,1,"Nome do arquivo :               ");
   LeString(25,5,13,av);
   switch ( tecpc )
     {
      case _esc   : LimpaQuadro(5,4,31,1);
                    return;
      case _F2    : if ( tecfunc )
                      EscolheExt(ExtArq(av));
                    if ( tecpc == _esc )
                      {
                       LimpaQuadro(5,4,31,1);
                       return;
                      }
                    break;
      default     : Copia(nome_arq,Maiuscula(av));
                    break;
     }
   Copia(na,NomeArq(nome_arq));
   EscrNomeArq();
   EscrXY(25,5,ComplVazio(nome_arq,13));
   nome_arq[PosFimSpc(nome_arq)+1] = '\0';
   Copia(ext,ExtArq(nome_arq));
   if ( Compara(ext,"DGM") ) 
     {
      arq_log = true;
      Copia(na,nome_arq);
      fp = NULL;
     }
     else
     if ( Compara(ext,"LGC") )
       {
        arq_lad = true;
        Copia(na,nome_arq);
        fp = NULL;
       }
       else
       fp = fopen(nome_arq,"r");
   if ( !fp )
     {
      if ( !arq_log && !arq_lad )
        {
         Quadro(10,9,26,1,"Arquivo texto inexistente.");
         Concat(na,".DGM");
         arq_log = true;
        }
      cont_buf = LerArqNorm(na,0,MAXBUFF);
      if ( !cont_buf )
        {
         Copia(na,NomeArq(na));
         Concat(na,"LGC");
         cont_buf = LerArqNorm(na,0,MAXBUFF);
         arq_log = false;
         arq_lad = true;
        }
      if ( !cont_buf )
        Quadro(15,13,38,1,"Arquivo Programa Usuario Inexistente.");
        else
        {
         if ( arq_log ) /* Decompilando arquivo antigo tipo *.DGM */
           {
            VerificaHeap(10);
            ZeraComp();
            VerificaHeap(11);
            ZeraBufTexto();
            VerificaHeap(12);
            InicDadosTexto();
            VerificaHeap(13);
            Quadro(28,10,19,3,"  Decompilando...    Endr. :    0       Linhas:    0     ");
            Decompilar();
            VerificaHeap(14);
            Copia(na,NomeArq(na));
            Copia(nome_arq,na);
            Concat(nome_arq,".LST");
            flag_ladder = false;
           }
           else     /* Arquivo LGC  - LADDER */
           {
            VerificaHeap(10);
            ZeraComp();
            VerificaHeap(11);
            ZeraBufTexto();
            VerificaHeap(12);
            VerifArqDat();
            VerificaHeap(13);
            AjustDadLadder();
            VerificaHeap(14);
            VerificaCfgProg();
            Copia(na,NomeArq(na));
            Copia(nome_arq,na);
            Concat(nome_arq,".LGC");
            Quadro(18,14,22,1,"Leitura Arquivo OK!");
           }
        }
     }
     else
     {
      Quadro(10,9,22,2," Lendo Arquivo...      Linhas lidas: 0      ");
      Concat(na,".LGC");
      cont_buf = LerArqNorm(na,0,MAXBUFF);
      ZeraComp();
      ZeraBufTexto();
      InicDadosTexto();
      VerificaHeap(1);
      VerifMem();
      EscrStatusMem();
      while (!fim_arq && (cont_lin <  MAXLINTXT) && (tecpc != _esc) && flag_memok)
        {
         
         if ( !(fgets(bufftxt,255,fp)) )
           fim_arq = true;
           else
           {
            ll = strlen(bufftxt);
            if ( ll < 79 )
              {
               ll--;
               if (bufftxt[ll] == 10)
                 bufftxt[ll] = '\0';
              }
            bufftxt[78] = '\0';
            if ( !(char)(PosFimSpc(bufftxt)+1) )
              Copia(bufftxt,";");
            if ( texto[cont_lin] )
              MensErr(MENSTXT);
            texto[cont_lin] = (char *) malloc(strlen(bufftxt)+1);
            if ( !texto[cont_lin] )
              MensErr(MALOC);
            Copia(texto[cont_lin++],bufftxt);
            EscrXY(26,11,StrW(cont_lin));
            VerifMem();
            EscrStatusMem();
           }
        }
      fim_lin = cont_lin - 1;
      fclose(fp);
      flag_ladder = false;
     }
   LeTecla();
   LimpaQuadro(5,4,38,1);
   LimpaQuadro(10,9,45,6);
   LimpaQuadro(28,10,19,3);
   LimpaQuadro(25,18,31,1);
  }

char near VerifArqExist(char *na)
  {
   FILE *fp;
   
   if ( (fp = fopen(na,"r")) != NULL )
     {
      Quadro(10,9,22,2," Arquivo ja existe...  Escreve sobre ? (S/N)");
      LeTecla();
      LimpaQuadro(10,9,22,2);
      if ( (tecpc != 's') && (tecpc != 'S') )
        {
         fclose(fp);
         LimpaQuadro(5,5,31,1);
         return(true);
        }
     }
   fclose(fp);
   return(false);
  }

char near SalvarArqBuffer(void)
  {
   register unsigned int tt,tr;
   char nome_arq_tab[40],
        err = false;
   FILE *fb;

   //Nome do arquivo com a .lgc
   Copia(nome_arq_tab,path);
   Concat(nome_arq_tab,NomeArq(nome_arq));
   Concat(nome_arq_tab,".LGC");

   //Verifica se ja existe um com esse nome
   if ( flag_ladder && VerifArqExist(nome_arq_tab) )
     return(true);

   //Abre o File e coloca na variavel fb
   fb = fopen(nome_arq_tab,"wb");
   tt = fim_buffer - inic_buffer + 1;
   tr = fwrite(&buffer[inic_buffer],1,tt,fb);
   if (tr != tt)
     err = true;
   fclose(fb);
   return(err);
  }

//Comando do Bot�o Arquivo > Gravar
void near SalvarArq(void)
  {
	register unsigned int tt;
	register unsigned int cbuf,
                     cont_lin = 0;
   char as[40],
       ll,
       err;
   FILE *fp;
   int coe = 0;

   

   Copia(as,nome_arq);
   Quadro(5,5,31,1,"Nome do arquivo:               ");
   LeString(25,6,13,as);
   if ( tecpc == _esc )
     {
      LimpaQuadro(5,5,31,1);
      return;
     }
   Copia(nome_arq,Maiuscula(as));
   EscrNomeArq();
   EscrXY(25,6,ComplVazio(nome_arq,13));
   
   //Caso n�o sei o q, mas tem a ver com o Ladder
   //if ( !flag_ladder )
   
   //Rhaast

   

   printf("\n\n\n\n\n\n\n");
   printf("End  Tipo\n");
	   //Abre o File e coloca na variavel fb
	   //fb = fopen(nome_arq_tab, "wb");

	
	   //Imprime os buffers at� chegar nos ff
		do
		{
		   printf(" %3d\t%3d\n", buffer[coe], buffer[coe + 1]);
		   coe += 2;
		} while (buffer[coe] != 255 && buffer[coe+1] != 255);
	

   if (1 == 2)
     {
      if ( VerifArqExist(nome_arq) )
		return;
      
	  //printf("Entrou aq");
	  fp = fopen(nome_arq,"w");
      Quadro(10,9,22,2," Salvando Arquivo...   Total Linhas :  xxxx   ");
      CorTexto(vermelho);
	  
	  while( (tecpc != _esc) && (cont_lin <= fim_lin) )
        {
         if ( TeclaPress() )
           LeTecla();

         VerifLinhaVazia(cont_lin);
         Copia(bufftxt,texto[cont_lin]);
         ll = PosFimSpc(bufftxt);
		 
		 if ( !(char)(ll+1) ){
            bufftxt[0] = ';';
            ll++;
           }

		 bufftxt[ll+1] = '\n';
         bufftxt[ll+2] = '\0';
         fputs(bufftxt,fp);
         bufftxt[ll+1] = '\0';
         free(texto[cont_lin]);
         texto[cont_lin] = (char *) malloc(ll+2);
		 
		 if ( !texto[cont_lin] )
           MensErr(MALOC);
         Copia(texto[cont_lin++],bufftxt);
         EscrXY(28,11,StrW(cont_lin));
      }

		

	  return;

      fclose(fp);
      CorTexto(azul);
      if ( tecpc != _esc )
        Quadro(25,14,22,1,"Arquivo TEXTO Salvo...");
     }
   if ( tecpc != _esc )
     {
      err = SalvarArqBuffer();
      if (!err)
        {
         SalvarArqDat();
         Quadro(36,18,22,1,"Arquivo LGC Salvo...");
        }
     }
   if ( !err || !flag_ladder )
     LeTecla();
   LimpaQuadro(5,5,31,1);
   LimpaQuadro(10,9,22,2);
   LimpaQuadro(25,14,22,1);
   LimpaQuadro(36,18,22,1);
  }

void near ImprLin(char *lin,FILE *prn)
  {
   register int ll;
   Copia(bs,lin);
   ll = strlen(bs);
   bs[ll++] = LF;
   bs[ll++] = CR;
   fwrite(&bs,ll,1,prn);
  }


void near ImprLin12(FILE *prn)
  {
   Copia(bufftxt,lin_impr1);
   Concat(bufftxt,lin_impr2);
   ImprLin(bufftxt,prn);
  }

void near ImprCabecalho(char pag,char *datai,char *horai,FILE *prn)
  {
   Copia(bufftxt,"***    LOGICON AUTOMACAO INDUSTRIAL LTDA.    ***      Programa: ");
   Concat(bufftxt,nome_arq);
   Concat(bufftxt,"         ");
   Concat(bufftxt,datai);
   Concat(bufftxt,"           ");
   Concat(bufftxt,horai);
   Concat(bufftxt,"       pag. ");
   Concat(bufftxt,Str(pag));
   ImprLin(bufftxt,prn);
  }

/******** Variaveis Impressao Ladder *********/

char  lin1[255],lin2[255],lin3[255];
char  flagth,flagtr,flagct,flagbh,flagbf;
const char tpln[2][9] = { "        ","--------" };

char near ExisteContato(int nc,int *ctt,int inter)
  {
   register int cc = 0;
   for ( ; cc < nc ; cc++ )
     if ( inter == ctt[cc] )
       return(true);
   return(false);
  }

int near LinJump(int endr)
  {
   register int cc = 0;
   while ( (cc < lin_prog) && (endr != (*linha[cc]).endr) )
     cc++;
   return (cc+1);
  }

int near JustNum(int nn)
  {
   if ( nn > 9999 )
     return(0);
   if ( nn > 999 )
     return(1);
   if ( nn > 99 )
     return(2);
   if ( nn > 9 )
     return(3);
   return(4);
  }


char near ProcContato(int yd,int *nc,int *ctt)
  {
   register int cc,ccc;
   int inter;
   char fl = false;
   for ( cc = 0 ; cc < 2*MAXCTTX ; cc++ )
     if ( cll[cc][yd] )
       {
        fl = true;
        inter = ill[cc][yd];
        switch ( cll[cc][yd] )
          {
           case _A :
           case _F : if ( !ExisteContato(*nc,ctt,inter) )
                       *(ctt+(*nc)++) = inter;
                     break;
           case _J :
           case _S : switch ( cll[cc+1][yd] )
                       {
                        case _TELR : inter |= 0x0400;
                        case _OUT  :
                        case _OUTI :
                        case _SET  :
                        case _RES  :
                        case _MONU :
                        case _MOND : if ( !ExisteContato(*nc,ctt,inter) )
                                       *(ctt+(*nc)++) = inter;
                                     break;
                        case _JMP  :
                        case _JPC  :
                        case _CALL :
                        case _CALC : *(ctt+(*nc)++) = -LinJump(inter);
                                     break;
                       }
                     break;
           case _B : if ( cll[cc-1][yd] != _B )
                       {
                        for ( ccc = 0 ; ccc < 2 ; ccc++ )
                          {
                           inter = ill[cc][yd+ccc];
                           switch ( inter )
                             {
                              case _ENDB :
                              case _RLDR :
                              case _SBLK :
                              case _LBLK :
                                           break;
                              default    : inter = ill[cc+2][yd+ccc] | 0x0400;
                                           if ( (ill[cc+1][yd+ccc] != IMED) && !ExisteContato(*nc,ctt,inter) )
                                             *(ctt+(*nc)++) = inter;
                                           break;
                             }
                          }
                       }
                     break;
          }
       }
   return(fl);
  }

void near IDesenhaInterno(xd,yd)
  {
   register int cc;
   int vl,ll;
   vl =  ill[xd][yd];
   switch ( ill[xd+1][yd] )
     {
      case '#' : if ( vl > 99 )
                   Concat(lin1," ");
                   else
                   if ( vl > 9 )
                     Concat(lin1,"  ");
                     else
                     Concat(lin1,"   ");
                 Copia(palavra,Str((char)vl));
                 break;
      default  : switch ( cll[xd+1][yd] )
                   {
                    case _OUTI :
                    case _TELR : vl &= 0x3FF;
                                 break;
                   }
                 if ( !cont_def || !ProcDef(auxp,vl) )
                   {
                    Copia(palavra,"  ");
                    Concat(palavra,DecHexW(vl));
                    *(palavra+2) = (char)ill[xd+1][yd];
                    Concat(palavra,"  ");
                   }
                   else
                   {
                    for ( ll = 0 ; (ll < 8) && ( *(auxp+ll) != ' ' ) ; ll++ );
                    for ( cc = 0 ; cc < (8 - ll)/2 ; cc++ )
                      *(palavra+cc) = ' ';
                    *(palavra+cc) = '\0';
                    Concat(palavra,auxp);
                    *(palavra+8) = '\0';
                   }
                 break;
     }
   Concat(lin1,palavra);
  }


void near IDesenhaLinha(int c)
  {
   switch ( c )
     {
      case 1 : Concat(lin1,tpln[1]);
	       Concat(lin2,tpln[0]);
               break;
      case 2 : Concat(lin1,tpln[0]);
	       Concat(lin2,tpln[1]);
	       Concat(lin3,tpln[0]);
               break;
      case 3 : Concat(lin2,tpln[0]);
	       Concat(lin3,tpln[1]);
               break;
     }
  }

void near IDesenhaCAberto(int xd,int yd)
  {
   Concat(lin2,"---| |--");
   Concat(lin3,tpln[0]);
   IDesenhaInterno(xd,yd);
  }

void near IDesenhaCFechado(int xd,int yd)
  {
   Concat(lin2,"---|/|--");
   Concat(lin3,tpln[0]);
   IDesenhaInterno(xd,yd);
  }

void near IDesenhaBobina(int xd,int yd)
  {
   int auxi;
   Concat(lin2,"--(  )--  ");
   if ( cll[xd][yd] != _J )
     {
      Concat(lin3,tpln[0]);
      IDesenhaInterno(xd,yd);
     }
     else
     switch ( cll[xd+1][yd] )
       {
        case _END  :
        case _RET  :
	case _RETC : Concat(lin3,tpln[0]);
                     break;
        case _JMP  :
        case _JPC  :
        case _CALL :
        case _CALC : auxi = LinJump(ill[xd][yd]);
                     Copia(palavra,"L : ");
                     Concat(palavra,StrW(auxi)+JustNum(auxi));
                     Concat(lin3,ComplVazio(palavra,8));
                     break;
       }
   Concat(lin1,"  ");
   Concat(lin3,"  ");
  }

void near IDesenhaC(int c)
  {
   register int cpr;
   char  c1,c2,c3;
   switch ( c )
     {
      case 0 : c1 = ' ';
               c2 = '+';
               c3 = '|';
               break;
      case 1 : c1 = ' ';
               c2 = '-';
               c3 = ' ';
               break;
      case 2 : c1 = '|';
               c2 = '|';
               c3 = '|';
               break;
      case 3 : c1 = '|';
               c2 = '|';
               c3 = '|';
               break;
      case 4 : c1 = '|';
               c2 = '+';
               c3 = ' ';
               break;
      case 5 : c1 = '|';
               c2 = '+';
               c3 = ' ';
               break;
      case 6 :
      case 7 : c1 = '|';
               c2 = '|';
               c3 = '|';
               break;
      case 8 : c1 = ' ';
               c2 = ' ';
               c3 = ' ';
               break;
      case 9 : c1 = '+';
               c2 = '|';
               c3 = '|';
               break;
      case 10: c1 = '-';
               c2 = ' ';
               c3 = ' ';
               break;
      case 11: c1 = '+';
               c2 = '|';
               c3 = '|';
               break;
      case 12: c1 = '|';
               c2 = '|';
               c3 = '|';
               break;
      case 13: c1 = '|';
               c2 = '|';
               c3 = '|';
               break;
      case 14: c1 = ' ';
               c2 = ' ';
               c3 = '-';
               break;
      case 15: c1 = '|';
               c2 = '|';
               c3 = '+';
               break;
      case 16: c1 = '|';
               c2 = '|';
               c3 = '+';
               break;
      case 17: c1 = '|';
               c2 = '|';
               c3 = '+';
               break;
      case 18: c1 = '|';
               c2 = '|';
               c3 = '+';
               break;
     }
   cpr = ComprStr(lin1);
   *(lin1+cpr) = c1;
   *(lin2+cpr) = c2;
   *(lin3+cpr++) = c3;
   *(lin1+cpr) = '\0';
   *(lin2+cpr) = '\0';
   *(lin3+cpr) = '\0';
  }

void near IDesenhaSaidas(int xd,int yd)
  {
   int cp;
   IDesenhaBobina(xd,yd);
   cp = ComprStr(lin2);
   switch ( cll[xd+1][yd] )
     {
      case _SET  : *(lin2+cp-7) = 'S';
                   *(lin2+cp-6) = 'T';
                   break;
      case _RES  : *(lin2+cp-7) = 'R';
                   *(lin2+cp-6) = 'S';
                   break;
      case _MONU : *(lin2+cp-7) = 'M';
                   *(lin2+cp-6) = 'U';
                   break;
      case _MOND : *(lin2+cp-7) = 'M';
                   *(lin2+cp-6) = 'D';
                   break;
      case _TELR :
      case _TEL  : *(lin2+cp-7) = 'T';
                   *(lin2+cp-6) = 'L';
                   break;
      case _END  :
      case _JMP  :
      case _JPC  :
      case _CALL :
      case _CALC :
      case _RET  :
      case _RETC : Concat(lin1,tpln[0]);
                   if ( cll[xd][yd] != _J )
                     break;
                   switch ( cll[xd+1][yd] )
                     {
                      case _END  : *(lin1+cp-8) = 'E';
                                   *(lin1+cp-7) = 'N';
                                   *(lin1+cp-6) = 'D';
                                   break;
                      case _JMP  : *(lin1+cp-8) = 'J';
                                   *(lin1+cp-7) = 'M';
                                   *(lin1+cp-6) = 'P';
                                   break;
                      case _JPC  : *(lin1+cp-8) = 'J';
                                   *(lin1+cp-7) = 'P';
                                   *(lin1+cp-6) = 'C';
                                   break;
                      case _CALL : *(lin1+cp-8) = 'C';
                                   *(lin1+cp-7) = 'A';
                                   *(lin1+cp-6) = 'L';
                                   *(lin1+cp-5) = 'L';
                                   break;
                      case _CALC : *(lin1+cp-8) = 'C';
                                   *(lin1+cp-7) = 'A';
                                   *(lin1+cp-6) = 'L';
                                   *(lin1+cp-5) = 'C';
                                   break;
                      case _RET  : *(lin1+cp-8) = 'R';
                                   *(lin1+cp-7) = 'E';
                                   *(lin1+cp-6) = 'T';
                                   break;
                      case _RETC : *(lin1+cp-8) = 'R';
                                   *(lin1+cp-7) = 'E';
                                   *(lin1+cp-6) = 'T';
                                   *(lin1+cp-5) = 'C';
                                   break;
                     }
                   break;
     }
  }


void near IContatoNulo(void)
  {
   Concat(lin1,tpln[0]);
   Concat(lin2,tpln[0]);
   Concat(lin3,tpln[0]);
  }


void near VerifOperBlk(int axd,int ayd,char *lin)
  {
   register int cc = 0,ccc;
   static int opr;
   *palavra = '\0';
   if ( cll[axd-1][ayd] != _B )
     {
      while ( ill[axd][ayd] != opreg[cc++] );
      opr = opreg[--cc];
      if ( opr == _ENDB )
	Concat(lin,tpln[0]);
        else
        {
         Concat(lin,"    ");
	 Copia(palavra,comando_registro[cc]);
         Concat(lin,ComplVazio(palavra,4));
        }
     }
     else
     switch ( ill[axd-2][ayd] )
       {
        case _ENDB :
        case _RLDR :
        case _SBLK :
	case _LBLK : Concat(lin,tpln[0]);
                     break;
        default    : switch ( ill[axd-1][ayd] )
                       {
                          case IMED : Copia(auxp,StrW(ill[axd][ayd]));
                                      ccc = 0;
                                      while( *(auxp + ccc) == ' ' )
                                        ccc++;
                                      Concat(palavra,auxp+ccc);
                                      break;
                          case INDX : Concat(palavra,"&");
                          case REG  : Concat(palavra,"R");
                                      Concat(palavra,Extrai(DecHexW(ill[axd][ayd]),1,3));
                                      break;
                       }
                     Concat(lin,ComplVazio(palavra,8));
                     break;

         }
  }


char near MontaLinhaImpr(int yd)
  {
   register int cliga,cc,ccc;
   static char nt,bt;
   int contato,liga,xd;
   char flag,flagr = false;

   for (xd = 0 ; xd < MAXCTTX*2 ; xd++)
     {
      contato = cll[xd][yd];
      liga = cll[xd+1][yd];
      flag = false;
      if ( contato != _N )
        {
         flagr = flag = true;
         switch (contato)
           {
            case _A  : IDesenhaCAberto(xd,yd);
                       break;
            case _F  : IDesenhaCFechado(xd,yd);
                       break;
            case _J  :
            case _S  : IDesenhaSaidas(xd,yd);
                       flag = false;
                       break;
            case _L  : IDesenhaLinha(2);
                       break;
            case _T  : if ( cll[xd][yd-1] == _N )
                         {
                          IDesenhaLinha(1);
                          cc = ComprStr(lin2);
                          if ( !flagth )
                            {
                             flagth = true;
                             *(lin2+cc-7) = 'H';
                             Concat(lin3,"    TIMR");
                            }
                            else
                            {
                             *(lin2+cc-2) = 'D';
                             Concat(lin3,"#  ");
                             nt = (char)ill[xd-2][yd];
                             Concat(lin3,DecHex(nt));
                             Concat(lin3,"   ");
                            }
                         }
                         else
                         if ( cll[xd][yd+1] == _N )
                           {
                            IDesenhaLinha(3);
                            cc = ComprStr(lin2);
                            if ( !flagtr )
                              {
                               flagtr = true;
                               *(lin2+cc-7) = 'R';
                               if ( !flagct )
                                 {
                                  bt = *(buffer+TAB_CFG_TIMER+nt);
                                  if ( bt & 0x01 )
                                   Concat(lin1,"    UP  ");
                                   else
                                   Concat(lin1,"    DOWN");
                                 }
                                 else
				 Concat(lin1,tpln[0]);
                              }
                              else
                              {
                               *(lin2+cc-2) = 'A';
                               if ( !flagct )
                                 {
                                  if ( bt & 0x02 )
                                    Concat(lin1,"0.1     ");
                                    else
                                    Concat(lin1,"0.01    ");
                                  flagct = true;
                                 }
                                 else
				 Concat(lin1,tpln[0]);
                              }
                           }
                           else
                           {
                            if ( cll[xd-2][yd] != _T )
                              {
                               if ( !flagct )
                                 {
                                  bt = *(buffer+TAB_CFG_TIMER+nt);
                                  if ( bt & 0x01 )
                                    Concat(lin1,"    UP  ");
                                    else
                                    Concat(lin1,"    DOWN");
                                 }
                                 else
				 Concat(lin1,tpln[0]);
                              }
                              else
                              {
                               if ( !flagct )
                                 {
                                  if ( bt & 0x02 )
                                    Concat(lin1,"0.1     ");
                                    else
                                    Concat(lin1,"0.01    ");
                                  flagct = true;
                                 }
                                 else
				 Concat(lin1,tpln[0]);
                              }
			    Concat(lin2,tpln[0]);
			    Concat(lin3,tpln[0]);
                           }
                       break;
            case _B  : if ( cll[xd][yd-1] != _B )
                         {
                          IDesenhaLinha(1);
                          cc = ComprStr(lin2);
                          if ( !flagbh )
                            {
                             flagbh = true;
                             *(lin2+cc-7) = 'H';
			     Concat(lin3,tpln[0]);
                            }
                            else
                            {
                             flagbh = false;
                             *(lin2+cc-2) = 'S';
			     Concat(lin3,tpln[0]);
                            }
                         }
                         else
                         {
                          if ( cll[xd][yd+1] != _B )
			    Concat(lin3,tpln[1]);
                            else
			    Concat(lin3,tpln[0]);
                          VerifOperBlk(xd,yd-1,lin2);
                          VerifOperBlk(xd,yd-2,lin1);
                         }
                       break;
           }
        }
    xd++;
    if ( flag )
      {
       switch ( liga )
         {
          case _L  : cliga = 1;
                     break;
          case _PC : switch ( cll[xd][yd+1] )
                       {
                        case _PR :
                        case _N  : cliga = 4;
                                   break;
                        case _P  :
                        case _PC : cliga = 2;
                                   break;
                       }
                     break;
          case _PR :
          case _P  : switch ( cll[xd+1][yd] )
                       {
                        case _T  : if ( (cll[xd+1][yd-1] != _T) || (cll[xd+1][yd+1] != _T) )
                                     {
                                      cliga = 0;
                                      break;
                                     }
                                   cliga = ( cll[xd][yd+1] == _N ) ? 4 : 2;
                                   break;
                        case _B  : if ( (cll[xd+1][yd-1] != _B) && (cll[xd+1][yd+1] == _B) )
                                     {
                                      cliga = 0;
                                      break;
                                     }
                        case _N  : cliga = ( cll[xd][yd+1] == _N ) ? 4 : 2;
                                   break;
                        default  : cliga = 0;
                                   break;
                       }
                     break;
          case _PP : cliga = 0;
                     break;
          case _PPR: cliga = 1;
                     break;
          case _BPR: if ((cll[xd-1][yd+1] == _T) || (cll[xd-1][yd+1] == _B))
                       cliga = 7;
                       else
                       cliga = (cll[xd-1][yd] == _T) ? 16 : 18;
                     break;
          case _BPPR:
          case _B  :
          case _T  : switch ( cll[xd+1][yd] )
                       {
                        case _T   : cliga = 8;
                                    if ( cll[xd][yd-1] != _T )
                                      cliga = 10;
                                      else
                                      if ( cll[xd][yd+1] != _T )
                                        cliga = 14;
                                    break;
                        case _B   : cliga = 8;
                                    if ( cll[xd][yd-1] != _B )
                                      cliga = 10;
                                      else
                                      if ( cll[xd][yd+1] != _B )
                                        cliga = 14;
                                    break;
                        case _N   : cliga = ( (cll[xd][yd+1] == _N) && (liga == _B) ) ? 18 : 7;
                                    break;
                        case _BPR :
                        case _L   :
                        case _A   :
                        case _F   :
                        case _J   :
                        case _S   : cliga = ( (cll[xd][yd-1] == _N) || ((liga == _B) && (liga == _BPPR)) ) ? 11 : 16;
                                    break;
                       }
                     break;
         }
       IDesenhaC(cliga);
       switch ( liga )
         {
          case _L   :
          case _P   : switch( cll[xd+1][yd] )
                        {
                         case _N  : cliga = 8;
                                    break;
                         case _J  :
                         case _S  :
                         case _L  :
                         case _A  :
                         case _F  : cliga = 1;
                                    break;
                         case _T  : if ( cll[xd+1][yd-1] != _T )
                                      cliga = 9;
                                      else
                                      if ( cll[xd+1][yd+1] != _T )
                                        cliga = 15;
                                        else
                                        cliga = 7;
                                      break;
                         case _B  : if ( cll[xd+1][yd+1] == _B )
                                      cliga = (cll[xd+1][yd-1] != _B) ? 9 : 7;
                                      else
                                      cliga = 17;
                                    break;
                        }
                      break;
          case _BPPR:
          case _PPR :
          case _PP  : cliga = 0;
                      break;
          case _BPR :
          case _PC  : if ( cll[xd+1][yd] != _N )
                        switch ( cll[xd][yd+1] )
                          {
                           case _BPR:
                           case _PC :
                           case _PR : cliga = 3;
                                      break;
                           default  : cliga = 5;
                                      break;
                          }
                        else
                        cliga = 7;
                      break;
          case _T   : switch ( cll[xd+1][yd] )
                        {
                         case _L :
                         case _A :
                         case _F :
                         case _J :
                         case _S : cliga = 1;
                                   break;
                         default : cliga = 8;
                                   if ( cll[xd][yd-1] != _T )
                                     cliga = 10;
                                     else
                                     if ( (cll[xd+1][yd] == _T) && (cll[xd][yd+1] != _T) )
                                       cliga = 14;
                                   break;
                        }
                      break;
          case _B   : switch ( cll[xd+1][yd] )
                        {
                         case _L :
                         case _A :
                         case _F :
                         case _J :
                         case _S : cliga = 1;
                                   break;
                         default : cliga = 8;
                                   if ( cll[xd][yd-1] != _B )
                                     cliga = 10;
                                     else
                                     if ( (cll[xd+1][yd] == _B) && (cll[xd][yd+1] != _B) )
                                       cliga = 14;
                                   break;
                        }
         }
       IDesenhaC(cliga);
      }
      else
      if ( (contato != _S) && (contato != _J) )
        {
         IContatoNulo();
         switch (liga)
           {
            case _N : IDesenhaC(8);
                      if ( (cll[xd+2][yd] == _B) || (cll[xd+2][yd] == _T) )
                        {
                         if ( cll[xd+2][yd] == _B )
                           cliga = ( cll[xd+2][yd+1] == _B ) ? 7 : 17;
                           else
                           cliga = 7;
                        }
                        else
                        cliga = 8;
                      IDesenhaC(cliga);
                      break;
            case _PP: IDesenhaC(0);
                      IDesenhaC(0);
                      break;
            case _PR: IDesenhaC(8);
                      if ( cll[xd+1][yd] != _N )
                        cliga = ( cll[xd][yd+1] == _N ) ? 5 : 3;
                        else
                        cliga = 7;
                      IDesenhaC(cliga);
                      break;
            case _P : switch ( cll[xd+1][yd] )
                        {
                         case _B :
                         case _T : IDesenhaC(7);
                                   IDesenhaC(7);
                                   break;
                         case _N : IDesenhaC(7);
                                   IDesenhaC(8);
                                   break;
                         default : cliga = ( cll[xd][yd+1] != _N ) ? 3 : 5;
                                   IDesenhaC(cliga);
                                   IDesenhaC(1);
                                   break;
                        }
                      break;
            case _BPR:
            case _PC: if ( cll[xd+1][yd] != _N )
                        {
                         if ( liga ==  _PC )
                           IDesenhaC(7);
                           else
                           IDesenhaC(8);
                         cliga = ( cll[xd][yd+1] == _PC ) ? 3 : 5;
                        }
                        else
                        {
                         IDesenhaC(7);
                         cliga = 7;
                        }
                      IDesenhaC(cliga);
                      break;
           }
        }
    }/**for**/
   return(flagr);
  }


void near RefContato(char *lin,int *nc,int ntc,int *ctt)
  {
   int aint;
   if ( *nc < ntc )
     {
      aint = *(ctt+(*nc));
      if ( aint >= 0 )
        {
         ProcDef(palavra,aint);
         Concat(palavra,"(");
         Copia(auxp,DecHexW(aint & 0x3FF));
         *auxp = VerifEISR(aint);
         Concat(palavra,auxp);
         Concat(palavra,") ");
        }
        else
        {
         Copia(palavra,"Label : ");
         Concat(palavra,PesqLabel(-aint-1,false));
        }
      Concat(lin,palavra);
      (*nc)++;
     }
  }


void near ImprLadder(char *di, char *hi, FILE *prn)
  {
   register int cc;
   int cl = 0,limpr,nc,ntc,ctt[100],auxyd,ayd;
   char chtip;


   CorTexto(azul);
   Quadro(20,12,22,3,"Impressao de Diagrama    Linha  :              Pagina :           ");
   chtip = FF;
   CorTexto(vermelho);
   do
     {
      limpr = 4;
      pag_txt++;
      ImprCabecalho(pag_txt,di,hi,prn);
      do
        {
         flagth = flagtr = flagct = flagbh = flagbf = false;
         nc = ntc = 0;
         EscrXY(36,14,StrW(cl+1));
         EscrXY(36,15,Str(pag_txt));
         LimpaLadder(0,false);
         auxyd = 0;
         MontaLadder(cl,&auxyd);
         for ( cc = 0 ; (cc < 2*MAXCTTY) && ProcContato(cc,&ntc,ctt) ; cc += 2 );
         cc /= 2;
         if ( ntc > 3*cc )
           limpr += (2 + ntc);
           else
           limpr += (3 + 3*cc);
         if ( limpr > 64 )
           break;
         Copia(bufftxt,"Linha ");
         Concat(bufftxt,StrW(cl+1)+JustNum(cl+1));
         Concat(bufftxt,"    -    Endr: ");
         Concat(bufftxt,DecHexW((*linha[cl]).endr));
         Concat(bufftxt,"h");
         Copia(auxp,PesqLabel(cl,false));
         if ( *auxp )
           {
            Concat(bufftxt,"   Label : ");
            Concat(bufftxt,auxp);
           }
         ImprLin(bufftxt,prn);
         *bufftxt = '\0';
         ImprLin(bufftxt,prn);
         ayd = 0;
         while ( ayd < MAXCTTY*2 )
           {
            *lin1 = *lin2 = *lin3 = '\0';
            if ( MontaLinhaImpr(ayd) )
              {
               RefContato(lin1,&nc,ntc,ctt);
               ImprLin(lin1,prn);
               RefContato(lin2,&nc,ntc,ctt);
               ImprLin(lin2,prn);
               RefContato(lin3,&nc,ntc,ctt);
               ImprLin(lin3,prn);
               ayd += 2;
              }
              else
              break;
           }
         while ( nc < ntc )
           {
            Copia(lin1,"  ");
            Concat(lin1,ComplVazio(lin1,108));
            RefContato(lin1,&nc,ntc,ctt);
            ImprLin(lin1,prn);
           }
         ImprLin(bufftxt,prn);
         cl++;
         if ( TeclaPress() )
           LeTecla();
        }while( (cl < lin_prog) && (tecpc != _esc) );
      fwrite(&chtip,1,1,prn);
     }while ( (cl < lin_prog) && (tecpc != _esc) );
  }


void near ImprLista(char *di,char *hi,FILE *prn)
  {
   register int cc;
   char linha,ll,
        chtip,fim_pag,
        flag_cmd;
   int  cont_impr,pcar,pspc;

   Copia(auxp,"( ");
   CorTexto(azul);
   Quadro(20,12,22,3,"Impressao de Relatorio   Linha  :              Pagina :           ");
   lin_comp = 0;
   chtip = FF;
   CorTexto(vermelho);
   do
     {
      linha = 2;
      pag_txt++;
      ImprCabecalho(pag_txt,di,hi,prn);
      do
        {
         linha++;
         Copia(edittxt,texto[lin_comp++]);
         Copia(bufftxt,"   ");
         flag_cmd = false;
         pcar = 0;
         SeparaPalavra(palavra,edittxt,&pcar,&pspc,80);
         if ( *palavra != ';' )
           {
            Copia(palavra,Maiuscula(palavra));
            cc = 0;
            while ( !flag_cmd && ( cc < MAXCMDBAS ) )
	      flag_cmd = ( Compara(palavra,comando_operando_basico[cc++]) ) ? true : false;
            cc = 0;
            while ( !flag_cmd && ( cc < MAXCMDREG ) )
	      flag_cmd = ( Compara(palavra,comando_registro[cc++]) ) ? true : false;
            if ( flag_cmd )
              {
               pcar = pspc;
               SeparaPalavra(palavra,edittxt,&pcar,&pspc,80);
               cc = 0;
               while ( (cc < cont_def) && !Compara(palavra,(*def[cc]).nome) )
                 cc++;
               if ( cc < cont_def )
                 {
                  Concat(bufftxt,auxp);
                  Copia(palavra,DecHexW((*def[cc]).inter & 0xFBFF));
                  *palavra = VerifEISR((*def[cc]).inter);
                  Concat(bufftxt,palavra);
                  Concat(bufftxt," )   ");
                 }
                 else
                 flag_cmd = false;
              }
           }
         if ( !flag_cmd )
           Concat(bufftxt,"           ");
         Concat(bufftxt,edittxt);
         ImprLin(bufftxt,prn);
         EscrXY(36,14,StrW(lin_comp));
         EscrXY(36,15,Str(pag_txt));
         delay(200);
         if ( TeclaPress() )
           LeTecla();
        } while( (lin_comp <= fim_lin) && (linha < 64) && (tecpc != _esc) );
      if ( tecpc != _esc )
        fwrite(&chtip,1,1,prn);
     } while( (lin_comp <= fim_lin) && (tecpc != _esc) );
   if ( tecpc != _esc )
     fwrite(&chtip,1,1,prn);
  }

void near ImprRefx(char *di,char *hi, FILE *prn)
  {
   register int cc;
   int cont_impr = 1,fim_pag = 64,linha,conti,
       alin,ll,ls,cntx_impr;
   char chtip,label_impr[9],inter_impr[5];

   CorTexto(vermelho);
   do
     {
      linha = 4;
      pag_txt++;
      ImprCabecalho(pag_txt,di,hi,prn);
      ImprLin12(prn);
      Copia(bufftxt,lin_impr3);
      ImprLin(bufftxt,prn);
      ImprLin12(prn);
      do
        {
         conti = 0;
         while( (conti < cont_refx) && ((*refx[conti]).inter != cont_impr) )
           conti++;
         if ( conti < cont_refx )
           {
            alin = (*refx[conti]).cntx / 18;
            linha += alin  + 1;
            if ( linha >= fim_pag )
              continue;
            Copia(label_impr,"  ****  ");
            for (cc = 0; cc < cont_def; cc++)
              if ( (*def[cc]).inter == cont_impr )
                {
                 Copia(label_impr,(*def[cc]).nome);
                 ll = strlen(label_impr);
                 for ( cc = ll; cc < 8; cc++ )
                   label_impr[cc] = ' ';
                 label_impr[cc] = '\0';
                 break;
                }
            chtip = VerifEISR(cont_impr);
            Copia(inter_impr,DecHexW(cont_impr & 0x3FF));
            inter_impr[0] = chtip;
            Copia(bufftxt,"| ");
            Concat(bufftxt,label_impr);
            Concat(bufftxt," | ");
            Concat(bufftxt,inter_impr);
            Concat(bufftxt," | ");
            ll = strlen(bufftxt);
            fwrite(&bufftxt,ll,1,prn);
            cntx_impr = 0;
            while( cntx_impr < (*refx[conti]).cntx )
              {
               cc = 1;
               while( (cntx_impr < (*refx[conti]).cntx) && (cc <= 18) )
                 {
                  alin = (*refx[conti]).linx[cntx_impr];
                  Copia(bufftxt,StrW(abs(alin)));
                  if ( alin < 0 )
                    {
                     alin = abs(alin);
                     if ( alin < 10 )
                       ls = 3;
                       else
                       if ( alin < 100 )
                         ls = 2;
                         else
                         if ( alin < 1000 )
                           ls = 1;
                           else
                           ls = 0;
                     bufftxt[ls] = '@';
                    }
                  fwrite(&bufftxt,5,1,prn);
                  cntx_impr++;
                  cc++;
                 }
               if ( (cntx_impr < (*refx[conti]).cntx) )
                 {
                  Copia(bufftxt,"  |xx");
                  bufftxt[3] = LF;
                  bufftxt[4] = CR;
                  fwrite(&bufftxt,5,1,prn);
                  Copia(bufftxt,lin_impr4);
                  fwrite(&bufftxt,strlen(bufftxt),1,prn);
                 }
                 else
                 {
                  Copia(bufftxt,"     ");
                  for (;cc <= 18;cc++)
                    fwrite(&bufftxt,5,1,prn);
                  Copia(bufftxt,"  |xx");
                  bufftxt[3] = LF;
                  bufftxt[4] = CR;
                  fwrite(&bufftxt,5,1,prn);
                 }
               EscrXY(36,14,inter_impr);
               EscrXY(36,15,Str((*refx[conti]).cntx));
               delay(200);
              }
           }
         if ( TeclaPress() )
           LeTecla();
         cont_impr++;
        } while( (cont_impr < 0x800) && (tecpc != _esc) && (linha < fim_pag) );
        if ( tecpc != _esc )
          {
           ImprLin12(prn);
           chtip = FF;
           fwrite(&chtip,1,1,prn);
          }
     } while( (cont_impr <= 2*MAXINTERNO+1) && (tecpc != _esc) );
   }


unsigned int VerifImpr(void)
  {
   union REGS regs;

   regs.h.ah = 0x02;
   regs.x.dx = 0x00;
   int86(0x17,&regs,&regs);
   return(regs.h.ah & 0x28);
  }


void near Impressao(void)
  {
   FILE *prn;
   char data_impr[14],hora_impr[9],esc_impr = 0,chtip;

   Janela(17,5,8,2,"Lista   Diagrama","LD",&esc_impr);
   LimpaQuadro(17,5,8,2);
   if ( tecpc == _esc )
     return;
   Quadro(18,8,27,2,"  Prepare a Impressora e,  digite <ENTER> p/ continuar");
   LeTecla();
   LimpaQuadro(18,8,27,2);
   if ( tecpc != _enter )
     return;
   prn = fopen("prn","wb");
   if ( VerifImpr() || !prn )
     {
      Quadro(23,9,19,1,"ERRO IMPRESSORA !");
      LeTecla();
      fclose(prn);
      LimpaQuadro(23,9,19,1);
      return;
     }
   Copia(data_impr,Data());
   Copia(hora_impr,Hora());
   Quadro(20,12,22,3,"Impr. Refer. Cruzada.  Contato  :            Num.Ocor.:           ");
   pag_txt = 0;
   if ( flag_compilado || flag_ladder )
     ImprRefx(data_impr,hora_impr,prn);
   if ( tecpc == _esc )
     {
      fclose(prn);
      LimpaQuadro(20,12,22,3);
      return;
     }
   chtip = 15;
   fwrite(&chtip,1,1,prn);
   switch ( esc_impr )
     {
      case 0 : if ( flag_ladder )
                 break;
               ImprLista(data_impr,hora_impr,prn);
               break;
      case 1 : ImprLadder(data_impr,hora_impr,prn);
               break;
     }
   CorTexto(azul);
   if ( tecpc != _esc )
     Copia(bufftxt,"Impressao Finalizada!  ");
     else
     Copia(bufftxt,"Impressao Interrompida!");
   Quadro(30,18,23,1,bufftxt);
   chtip = 18;
   fwrite(&chtip,1,1,prn);
   fclose(prn);
   LeTecla();
   LimpaQuadro(20,12,22,3);
   LimpaQuadro(30,18,23,1);
  }



void near Saida(void)
  {
   Quadro(10,12,41,1," Digite < ENTER > para abandonar COMPLOGI. ");
   LeTecla();
   sair = ( tecpc == _enter ) ? true : false;
   tecpc = 0;
   LimpaQuadro(10,12,41,1);
  }

void near Arquivos(void)
  {
   switch (esc_arq)
     {
      case 0 : LerArq();
               break;
      case 1 : SalvarArq();
               break;
      case 2 : Impressao();
               break;
      case 3 : Saida();
               break;
    }
  }


void near MonitoraEstInt(void)
  {
   char ch[4] = { ' ','0',' ','\0' };
   register int cc,it;
   CorFundo(cinza);
   CorTexto(vermelho);
   for ( cc = 0 ; cc < c_monit_ei ; cc++ )
     {
      it = *(monit_ei+cc);
      if ( it > MAXINTERNO )
        EscrXY(74,6+cc,StrW(reg_int[it & 0xFBFF]));
        else
        {
         *(ch+1) = af[est_int[it] & 0x01];
         if ( (est_int[it] & 0x10) )
           *ch = '*';
           else
           *ch = ' ';
         EscrXY(76,6+cc,ch);
        }
     }
  }


void near MolduraMonitES(void)
  {
   char es[2] = { 0,0 };
   int  ac,ne = inic_ed + offed*16,
           ns = inic_sd + offsd*16;
   register int cc;
   CorFundo(cinza);
   CorTexto(azul);
   *es = des;
   for ( cc = 0 ; cc < 16 ; cc++ )
     {
      ac = 3*cc + 4;
      EscrXY(ac,4,es);
      EscrXY(ac,7,es);
      EscrXY(ac,5,DecHex(cc+ns));
      EscrXY(ac,8,DecHex(cc+ne));
     }
  }

void near AtualizaSD(void)
  {
   register int cc,cp = 4,
                   sd = inic_sd + offsd*16;
   char ei[2] = { 0,0 };
   for ( cc = 0 ; cc < 16 ; cc++ )
     {
      *ei = ( est_int[sd+cc] & 0x01 ) ? lig : des;
      if ( (est_int[sd+cc] & 0x10) )
        {
         CorFundo(azul);
         CorTexto(branco);
        }
        else
        {
         CorFundo(cinza);
         CorTexto(azul);
        }
      EscrXY(cp,4,ei);
      cp += 3;
     }
  }

void near AtualizaED(void)
  {
   register int cc,
                ned = 16*offed,
                fed = ned + 16,
                ed = inic_ed + offed*16;
   for ( cc = 0; cc < fed + 16; cc++ )
     if ( !(est_int[ed+cc] & 0x10) )
       est_int[ed+cc] = buffed[ned+cc] ;
  }

void near MostraConfig(void)
  {
   char plce[] = "NE:     ";
   char plcs[] = "NS:     ";
   register int cc;
   for (cc = 0; cc < 4; cc++)
     {
      if ( cc < cfg_ed )
        plce[4+cc] = '#';
      if ( cc < cfg_sd )
        plcs[4+cc] = '#';
     }
   EscrXY(53,7,plce);
   EscrXY(53,4,plcs);
  }

void near ListMonitEI(void)
  {
   register int cc,cp,it;
   CorFundo(cinza);
   for ( cc = 0 ; cc < c_monit_ei ; cc++)
     {
      CorTexto(azul);
      cp = cc + 6;
      EscrXY(66,cp,ComplVazio(lab_monit_ei[cc],8));
      CorTexto(vermelho);
      if ( monit_ei[cc] <= MAXINTERNO )
        {
         *(bufftxt+3) = '\0';
         *(bufftxt+2) = ' ';
         *(bufftxt+1) = af[est_int[monit_ei[cc]] & 0x01];
         if ( est_int[monit_ei[cc]] & 0x10 )
           *bufftxt = '*';
           else
           *bufftxt = ' ';
         EscrXY(76,cp,bufftxt);
        }
        else
        EscrXY(74,cp,StrW(reg_int[monit_ei[cc] & 0xFBFF]));
     }
   cc += 6;
   for ( ; cc < 22 ; )
     EscrXY(66,cc++,"             ");
  }

void near InicializaCompilacao(void)
{
	register int cc, endrp;
	if (!acion_sim)
		VerifArqSim();
	for (cc = 0; cc < MAXTIMR; cc++)
	{
		temp[cc].codt.bit.h = 0;
		temp[cc].codt.bit.r = 0;
		temp[cc].codt.bit.a = 0;
		temp[cc].codt.bit.d = 0;
		temp[cc].ef = 0;
		temp[cc].codt.cod = buffer[TAB_CFG_TIMER + cc];
		if (!temp[cc].codt.bit.pre)
		{
			endrp = TAB_AUTO_LOAD + 2 * cc;
			temp[cc].pr = (int)((int)*(buffer + endrp) + (int)((int)*(buffer + endrp + 1) * 256));
		}
	}
	for (cc = 0; cc <= MAXINTERNO; cc++)
		est_int[cc] = 0;
	for (cc = 0; cc < 64; cc++)
		buffed[cc] = 0;
	c_pilha_bit = c_pilha_reg = cont_buf = work = c_pilha_endr = 0;
	fim_var = sair = false;
	cont_varr = 0;
	est_int[ONVARR1] = 0x11;
	est_int[ONALL] = 0x11;
	est_int[OFFALL] = 0x10;
}

void near InicializaSimulacao(void)
  {
   register int cc,endrp;
   if ( !acion_sim )
     VerifArqSim();
   for ( cc = 0 ; cc < MAXTIMR ; cc++ )
     {
      temp[cc].codt.bit.h = 0;
      temp[cc].codt.bit.r = 0;
      temp[cc].codt.bit.a = 0;
      temp[cc].codt.bit.d = 0;
      temp[cc].ef = 0;
      temp[cc].codt.cod = buffer[TAB_CFG_TIMER+cc];
      if ( !temp[cc].codt.bit.pre )
        {
         endrp = TAB_AUTO_LOAD + 2*cc;
         temp[cc].pr = (int)((int)*(buffer+endrp) + (int)((int)*(buffer+endrp+1)*256));
        }
     }
   for ( cc = 0 ; cc <= MAXINTERNO ; cc++ )
     est_int[cc] = 0;
   for ( cc = 0 ; cc < 64 ; cc++ )
     buffed[cc] = 0;
   c_pilha_bit = c_pilha_reg = cont_buf = work = c_pilha_endr = 0;
   fim_var = sair = false;
   cont_varr = 0;
   est_int[ONVARR1] = 0x11;
   est_int[ONALL]   = 0x11;
   est_int[OFFALL]  = 0x10;
  }

void near PushPilhaBit(char n, char ei)
  {
   register int cc,ccc;
   for ( cc = 0 ; cc < n ; cc++ )
     {
      for ( ccc = 15 ; ccc > 0 ; ccc-- )
        pilha_bit[ccc] = pilha_bit[ccc-1];
      pilha_bit[0] = ei;
     }
  }

void near PopPilhaBit(void)
  {
   register int cc;
   for ( cc = 0 ; cc < 14 ; cc++ )
     pilha_bit[cc] = pilha_bit[cc+1];
  }

void near PushPilhaReg(char n, int reg)
  {
   register int cc,ccc;
   for ( cc = 0 ; cc < n ; cc++ )
     {
      for ( ccc = 15 ; ccc > 0 ; ccc-- )
        pilha_reg[ccc] = pilha_reg[ccc-1];
      pilha_reg[0] = reg;
     }
  }

void near PopPilhaReg(void)
  {
   register int cc;
   for ( cc = 0 ; cc < 14 ; cc++ )
     pilha_reg[cc] = pilha_reg[cc+1];
  }

void near PushPilhaEndr(int endr)
  {
   register int cc;
   for ( cc = 0 ; cc < 7 ; cc++ )
     pilha_endr[cc+1] = pilha_endr[cc];
   pilha_endr[0] = endr;
  }

int near PopPilhaEndr(void)
  {
   register int cc;
   int endr = pilha_endr[0];
   for ( cc = 0 ; cc < 7 ; cc++ )
     pilha_endr[cc] = pilha_endr[cc + 1];
   return(endr);
  }

void near ComandoSalto(void)
  {
   switch ( oper )
     {
      case  _END  : fim_var = true;
                    cont_varr += CEND;
                    break;
      case  _JMP  : cont_buf = 256 * buffer[cont_buf+1] + buffer[cont_buf];
                    cont_varr += CJMP;
                    break;
      case  _JPC  : if ( *pilha_bit )
                      {
                       cont_buf = 256 * buffer[cont_buf+1] + buffer[cont_buf];
                       cont_varr += CJPC1;
                      }
                      else
                      {
                       cont_buf += 2;
                       cont_varr += CJPC0;
                      }
                    PopPilhaBit();
                    break;
      case _CALL :  cont_buf += 2;
                    PushPilhaEndr(cont_buf);
                    cont_buf = 256 * buffer[cont_buf-1] + buffer[cont_buf-2];
                    cont_varr += CCALL;
                    break;
      case _CALC :  cont_buf += 2;
                    if ( *pilha_bit )
                      {
                       PushPilhaEndr(cont_buf);
                       cont_buf = 256 * buffer[cont_buf-1] + buffer[cont_buf-2];
                       cont_varr += CCALC1;
                      }
                      else
                      cont_varr += CCALC0;
                    break;
      case _RET  :  cont_buf = PopPilhaEndr();
                    cont_varr += CRET;
                    break;
      case _RETC :  if ( *pilha_bit )
                      {
                       cont_buf = PopPilhaEndr();
                       cont_varr += CRETC1;
                      }
                      else
                      cont_varr += CRETC0;
                    PopPilhaBit();
                    break;
     }
  }

void near AnalizaComandoBasico(void)
  {
   aoper = 256 * moper + oper;
   switch ( opcod )
     {
      case _RLD  : PushPilhaBit(oper,*pilha_bit);
                   cont_varr += oper*CRLD;
                   break;
      case _LD   : PushPilhaBit(1,est_int[aoper] & 0x01);
                   cont_varr += CLD;
                   break;
      case _LDN  : PushPilhaBit(1,(est_int[aoper] & 0x01) ^ 01);
                   cont_varr += CLDN;
                   break;
      case _AND  : *pilha_bit &= (est_int[aoper] & 0x01);
                   cont_varr += CAND;
                   break;
      case _ANDN : *pilha_bit &= (est_int[aoper] ^ 0x01);
                   cont_varr += CANDN;
                   break;
      case _OR   : *pilha_bit |= est_int[aoper] & 01;
                   cont_varr += COR;
                   break;
      case _ORN  : *pilha_bit |= (est_int[aoper] & 01) ^ 01;
                   cont_varr += CORN;
                   break;
      case _ANDP : work = *pilha_bit;
                   PopPilhaBit();
                   *pilha_bit &= work;
                   cont_varr += CANDP;
                   break;
      case _ORP  : work = *pilha_bit;
                   PopPilhaBit();
                   *pilha_bit |= work;
                   cont_varr += CORP;
                   break;
      case _MONU : if ( !(est_int[aoper] & 0x10) )
                     {
                      if ( !(est_int[aoper] & 0x80) )
                        {
                         est_int[aoper] = 0;
                         est_int[aoper] |= (*pilha_bit << 7) | *pilha_bit;
                        }
                      else
                      est_int[aoper] &= *pilha_bit << 7;
                     }
                   PopPilhaBit();
                   cont_varr += CMONU;
                   break;
      case _MOND : if ( !(est_int[aoper] & 0x10) )
                     {
                      if ( !*pilha_bit )
                        {
                         if ( (est_int[aoper] & 0x80) )
                           est_int[aoper] = 01;
                           else
                           est_int[aoper] = 00;
                        }
                        else
                        est_int[aoper] = 0x80;
                     }
                   PopPilhaBit();
                   cont_varr += CMOND;
                   break;
      case _OUTI : aoper = reg_int[aoper];
                   cont_varr += COUTI;
      case _OUT  : if ( !(est_int[aoper] & 0x10) )
                     est_int[aoper] = *pilha_bit;
                   PopPilhaBit();
                   cont_varr += COUT;
                   break;
      case _SET  : if ( !(est_int[aoper] & 0x10) && *pilha_bit )
                     est_int[aoper] = 0x01;
                   PopPilhaBit();
                   cont_varr += CSET;
                   break;
      case _RES  : if ( !(est_int[aoper] & 0x10) && *pilha_bit )
                     est_int[aoper] = 0;
                   PopPilhaBit();
                   cont_varr += CRES;
                   break;
      case _JUMP : ComandoSalto();
     }
  }

char near *DadoTemp(char nt,int vt)
  {
   register int cc,cp;
   char as[7];
   if ( temp[nt].codt.bit.h/*tim*/ )
     {
      Copia(bs,StrW(vt));
      if ( vt < 100 )
        {
         if ( vt < 10 )
           {
            bs[3] = '0';
            if ( !temp[nt].codt.bit.dec )
              bs[2] = '0';
           }
           else
           if ( !temp[nt].codt.bit.dec )
             bs[1] = '0';

        }
      cp = ( temp[nt].codt.bit.dec ) ? 3 : 2;
      for ( cc = 0 ; cc < cp ; cc++ )
        bs[cc] = bs[cc+1];
      bs[cc] = '.';
     }
     else
     {
      Copia(as,Extrai(StrW(vt),1,4));
      Copia(bs,as);
     }
   return(bs);
  }

void near MoldTemp(void)
  {
   CorTexto(azul);
   CorFundo(cinza);
   MoldQuadro(2,12,15,10);
   EscrXY(3,13,"T/C  PRES.  EFET.");
  }

void near ListMonitTemp(void)
  {
   register int cc;
   char at[7];
   CorFundo(cinza);
   CorTexto(vermelho);
   for ( cc = 0 ; cc < cont_temp ; cc++ )
     {
      Copia(bufftxt,DecHex(monit_temp[cc]));
      Concat(bufftxt," : ");
      Concat(bufftxt,DadoTemp(monit_temp[cc],temp[monit_temp[cc]].pr));
      Concat(bufftxt," ");
      Concat(bufftxt,DadoTemp(monit_temp[cc],temp[monit_temp[cc]].ef));
      EscrXY(3,14+cc,bufftxt);
     }
  }

void near DeletaMonitTemp(void)
  {
   register int cc,ct = 0,cp;
   char at[7];
   CorFundo(verde);
   CorTexto(marrom);
   while ( (cont_temp > 0) && (tecpc != _esc) )
     {
      CorFundo(verde);
      CorTexto(marrom);
      EscrXY(3,14+ct,DecHex(monit_temp[ct]));
      LeTecla();
      CorFundo(cinza);
      CorTexto(vermelho);
      EscrXY(3,14+ct,DecHex(monit_temp[ct]));
      switch (tecpc)
        {
         case _up   : if ( ct > 0 )
                        ct--;
                        else
                        ct = cont_temp - 1;
                      break;
         case _down : if ( ct < cont_temp - 1 )
                        ct++;
                        else
                        ct = 0;
                      break;
         case _enter: cont_temp--;
                      if ( ct == cont_temp )
                        ct--;
                        else
                        for ( cc = ct ; cc < cont_temp ; cc++ )
                          monit_temp[cc] = monit_temp[cc+1];
                      ListMonitTemp();
                      EscrXY(3,14+cont_temp,ComplVazio(" ",16));
                      break;
        }
     }
  }

void near AnalizaComandoTempo(void)
  {
   char hab,aps,dur;
   hab = *(pilha_bit+1);
   aps = temp[oper].codt.bit.a;
   dur = temp[oper].codt.bit.d;
   temp[oper].codt.bit.r = *pilha_bit;
   PopPilhaBit();
   PopPilhaBit();
   temp[oper].codt.bit.h = hab;
   PushPilhaBit(1,aps);
   PushPilhaBit(1,dur);
   cont_varr += CTIMR;
  }

void near VerificaTempCont()
  {
   register int cc;
   char dec = false,
        cent = false;
   timerdec++;
   if ( timerdec > 190 )
     {
      timerdec = 0;
      dec = true;
     }
   timercent++;
   if ( timercent > 18 )
     {
      timercent = 0;
      cent = true;
     }
   for ( cc = 0 ; cc < MAXTIMR ; cc++ )
     {
      if ( !temp[cc].codt.bit.r )
        {
         if ( temp[cc].codt.bit.up )
           temp[cc].ef = 0;
           else
           temp[cc].ef = temp[cc].pr;
           temp[cc].codt.bit.a = temp[cc].codt.bit.d = 0;
        }
        else
        if ( temp[cc].codt.bit.h && !temp[cc].codt.bit.a )
          {
           if ( temp[cc].codt.bit.h/*tim*/ )
             {
              if ( temp[cc].codt.bit.dec )
                {
                 if ( dec )
                   {
                    if ( temp[cc].codt.bit.up )
                      temp[cc].ef++;
                      else
                      temp[cc].ef--;
                   }
                }
                else
                {
                 if ( cent )
                   {
                    if ( temp[cc].codt.bit.up )
                      temp[cc].ef++;
                      else
                      temp[cc].ef--;
                   }
                }
             }
             else /* contadores */
             {
              if ( !temp[cc].codt.bit.h/*c*/ )
                {
                 if ( temp[cc].codt.bit.up )
                   temp[cc].ef++;
                   else
                    temp[cc].ef--;
                 temp[cc].codt.bit.h/*c*/ = 1;
                }
             }
          }
          else
          if ( !temp[cc].codt.bit.h )
            temp[cc].codt.bit.h/*c*/ = 0;
      if ( !temp[cc].codt.bit.a && temp[cc].codt.bit.h )
        {
         temp[cc].codt.bit.d = temp[cc].codt.bit.r;
         if ( temp[cc].codt.bit.r )
           if ( temp[cc].codt.bit.up )
             {
              if ( temp[cc].ef >= temp[cc].pr )
                {
                 temp[cc].codt.bit.a = 1;
                 temp[cc].codt.bit.d = 0;
                }
             }
             else
             {
              if ( !temp[cc].ef )
                {
                 temp[cc].codt.bit.a = 1;
                 temp[cc].codt.bit.d = 0;
                }
             }
        }
     }
  }

void MensErrSim(void)
  {
   Quadro(29,10,14,2,"Erro Programa! Endr: xxxxh ");
   CorTexto(vermelho);
   EscrXY(38,12,DecHexW(cont_buf-2));
   LeTecla();
   LimpaQuadro(29,10,14,2);
  }

int ExtraiConstante(void)
  {
   register int ck;
   ck = buffer[cont_buf+1]*256 + buffer[cont_buf];
   cont_buf += 2;
   return(ck);
  }

char AnalizaComandoBloco(void)
  {
   register int cont_blk = oper,
                 vreg;
   if ( !(*pilha_bit) )
     {
      cont_buf += cont_blk;
      return(false);
     }
   while ( cont_blk )
     {
      oper  = buffer[cont_buf++];
      opcod = buffer[cont_buf++];
      aoper = (int) oper + ((opcod & 0xC0) << 2);
      opcod &= 0x3F;
      switch ( opcod )
        {
         case _RLDR : PushPilhaReg(oper,*pilha_reg);
                      cont_varr += oper*CRLDR;
                      cont_blk -= 2;
                      break;
         default    : switch ( opcod & 0xFE )
                        {
                         case  _MULR :
                         case  _DIVR :
                         case  _LT   :
                         case  _GT   :
                         case  _EQ   : if ( opcod & 0x01 )
                                         {
                                          vreg = reg_int[aoper];
                                          cont_varr += CREG;
                                         }
                                         else
                                         {
                                          vreg = ExtraiConstante();
                                          cont_blk -= 2;
                                          cont_varr += CIMED;
                                         }
                                       cont_blk -= 2;
                                       switch ( opcod & 0xFE )
                                         {
                                          case _MULR : *pilha_reg *= vreg;
                                                       cont_varr += CMULR;
                                                       break;
                                          case _DIVR : if ( vreg )
                                                         *pilha_reg /= vreg;
                                                         else
                                                         *pilha_reg = 9999;
                                                       cont_varr += CDIVR;
                                                       break;
                                          case _LT   : if ( *pilha_reg > vreg )
                                                         *pilha_bit = 0;
                                                       PopPilhaReg();
                                                       cont_varr += CLT;
                                                       break;
                                          case _GT   : if ( *pilha_reg < vreg )
                                                         *pilha_bit = 0;
                                                       PopPilhaReg();
                                                       cont_varr += CGT;
                                                       break;
                                          case _EQ   : if ( *pilha_reg != vreg )
                                                        *pilha_bit = 0;
                                                       PopPilhaReg();
                                                       cont_varr += CEQ;
                                                       break;
                                         }
                                       break;
                         case _INCR  :
                         case _DECR  :
                         case _OUTR  : switch ( (opcod & 0x01) + 1 )
                                         {
                                          case INDX : vreg = reg_int[aoper];
                                                      cont_varr += CINDX;
                                                      break;
                                          case REG  : vreg = aoper;
                                                      cont_varr += CREG;
                                                      break;
                                         }
                                       cont_blk -= 2;
                                       switch ( opcod & 0xFE )
                                         {
                                          case _INCR : reg_int[vreg]++;
                                                       cont_varr += CINCR;
                                                       break;
                                          case _DECR : reg_int[vreg]--;
                                                       cont_varr += CDECR;
                                                       break;
                                          case _OUTR : reg_int[vreg] = *pilha_reg;
                                                       PopPilhaReg();
                                                       cont_varr += COUTR;
                                                       break;
                                         }
                                       break;
                         default     : switch ( opcod & 0xFC )
                                         {
                                          case _LDR  :
                                          case _SUMR :
                                          case _SUBR : switch ( opcod & 0x03 )
                                                         {
                                                          case IMED : vreg = ExtraiConstante();
                                                                      cont_blk -= 2;
                                                                      cont_varr += CIMED;
                                                                      break;
                                                          case REG  : vreg = reg_int[aoper];
                                                                      cont_varr += CREG;
                                                                      break;
                                                          case INDX : vreg = reg_int[reg_int[aoper]];
                                                                      cont_varr += CINDX;
                                                                      break;
                                                         }
                                                       cont_blk -= 2;
                                                       switch ( opcod & 0xFC )
                                                         {
                                                          case _LDR  : PushPilhaReg(1,vreg);
                                                                       cont_varr += CLDR;
                                                                       break;
                                                          case _SUMR : *pilha_reg += vreg;
                                                                       cont_varr += CSUMR;
                                                                       break;
                                                          case _SUBR : *pilha_reg -= vreg;
                                                                       cont_varr += CSUBR;
                                                                       break;
                                                         }
                                                       break;
                                          default    : MensErrSim();
                                                       return(true);
                                         }
                                       break;
                        }
                      break;
        }
     }
   return(false);
  }

  char near AcionaCompilacao(void)
  {
	  char err_sim;
	  register int cc;
	  oper = buffer[cont_buf++];
	  opcod = buffer[cont_buf++];
	  moper = opcod >> 6;
	  opcod &= 0x3F;
	  switch (opcod)
	  {
	  case _RLD:
	  case _LD:
	  case _LDN:
	  case _AND:
	  case _ANDN:
	  case _OR:
	  case _ORN:
	  case _ORP:
	  case _ANDP:
	  case _MONU:
	  case _MOND:
	  case _SET:
	  case _RES:
	  case _OUT:
	  case _OUTI:
	  case _JUMP: AnalizaComandoBasico();
		  break;
	  case _TIMR: AnalizaComandoTempo();
		  break;
	  case _SEL:
	  case _INA:
	  case _OUTA: cont_buf += 2;
		  cont_varr += CESP;
		  break;
	  case _BLK: err_sim = AnalizaComandoBloco();
		  if (err_sim)
			  return(true);
		  break;
	  case _TEL: cont_buf += 2;
	  case _TELR: cont_buf += 2;
		  cont_varr += 2 * CESP;
		  break;
	  default: MensErrSim();
		  return(true);
	  }
	  if (fim_var)
	  {
		  ListMonitTemp();
		  MonitoraEstInt();
		  cont_varr >>= 3;
		  cont_varr += CSIS;
		  Copia(bufftxt, StrW(cont_varr));
		  for (cc = 6; cc > 3; cc--)
			  *(bufftxt + cc) = *(bufftxt + cc - 1);
		  *(bufftxt + cc--) = '.';
		  if (*(bufftxt + cc) == ' ')
			  *(bufftxt + cc) = '0';
		  EscrXY(47, 25, bufftxt);
		  cont_varr = cont_buf = 0;
		  fim_var = false;
		  est_int[ONVARR1] = 0x10;
		  est_int[ONALL] = 0x11;
		  est_int[OFFALL] = 0x10;
		  AtualizaSD();
		  AtualizaED();
	  }
	  VerificaTempCont();
	  return (false);
  }

char near AcionaSimulacao(void)
  {
   char err_sim;
   register int cc;
   oper = buffer[cont_buf++];
   opcod = buffer[cont_buf++];
   moper = opcod >> 6;
   opcod &= 0x3F;
   switch ( opcod )
     {
      case _RLD  :
      case _LD   :
      case _LDN  :
      case _AND  :
      case _ANDN :
      case _OR   :
      case _ORN  :
      case _ORP  :
      case _ANDP :
      case _MONU :
      case _MOND :
      case _SET  :
      case _RES  :
      case _OUT  :
      case _OUTI :
      case _JUMP : AnalizaComandoBasico();
                   break;
      case _TIMR : AnalizaComandoTempo();
                   break;
      case _SEL  :
      case _INA  :
      case _OUTA : cont_buf += 2;
                   cont_varr += CESP;
                   break;
      case _BLK  : err_sim = AnalizaComandoBloco();
                   if ( err_sim )
                     return(true);
                   break;
      case _TEL  : cont_buf += 2;
      case _TELR : cont_buf += 2;
                   cont_varr += 2*CESP;
                   break;
      default    : MensErrSim();
                   return(true);
     }
   if ( fim_var )
     {
      ListMonitTemp();
      MonitoraEstInt();
      cont_varr >>= 3;
      cont_varr += CSIS;
      Copia(bufftxt,StrW(cont_varr));
      for ( cc = 6 ; cc > 3 ; cc-- )
        *(bufftxt + cc) = *(bufftxt + cc - 1);
      *(bufftxt + cc--) = '.';
      if ( *(bufftxt + cc) == ' ' )
        *(bufftxt+cc) = '0';
      EscrXY(47,25,bufftxt);
      cont_varr = cont_buf = 0;
      fim_var = false;
      est_int[ONVARR1] = 0x10;
      est_int[ONALL]   = 0x11;
      est_int[OFFALL]  = 0x10;
      AtualizaSD();
      AtualizaED();
     }
   VerificaTempCont();
   return ( false );
  }


void near InsereEstInt(void)
  {
   register int cc;
   char cei,err,ei_ok,sair = false;
   int eiw;
   if ( c_monit_ei == 15 )
     return;
   Quadro(36,10,18,1,"Interno :         ");
   do
     {
      Copia(palavra,"        ");
      LeString(48,11,8,palavra);
      palavra[PosFimSpc(palavra)+1] = '\0';
      if ( tecpc != _enter )
        {
         LimpaQuadro(36,10,18,1);
         return;
        }
      cc = 0;
      while( !Compara(palavra,(*def[cc]).nome) && (cc < cont_def) && cont_def )
        cc++;
      if ( (cc < cont_def) && cont_def )
        {
         monit_ei[c_monit_ei] = (*def[cc]).inter;
         Copia(lab_monit_ei[c_monit_ei++],(*def[cc]).nome);
         sair = true;
        }
        else
        {
         Copia(palavra,Maiuscula(palavra));
         switch ( strlen(palavra) )
           {
            case 3 : Copia(bufftxt,"00");
                     Concat(bufftxt,Extrai(palavra,1,2));
                     break;
            case 4 : Copia(bufftxt,"0");
                     Concat(bufftxt,Extrai(palavra,1,3));
                     break;
           }
         eiw = HexDecW(bufftxt);
         if ( !error_conv && (eiw <= MAXINTERNO) )
           {
            if ( *palavra == 'R' )
              {
               eiw |= 0x400;
               *bufftxt = *palavra;
              }
              else
              *bufftxt = VerifEISR(eiw);
            monit_ei[c_monit_ei] = eiw;
            Copia(lab_monit_ei[c_monit_ei++],bufftxt);
            sair = true;
           }
        }
     } while( !sair );
   ListMonitEI();
   LimpaQuadro(36,10,18,1);
  }

void near ForcaEstInt(void)
  {
   register int cc;
   char cei,err,ei_ok,sair = false;
   int eiw,vreg;
   Quadro(36,10,18,1,"Interno :         ");
   do
     {
      Copia(palavra,"        ");
      LeString(48,11,8,palavra);
      palavra[PosFimSpc(palavra)+1] = '\0';
      if ( tecpc != _enter )
        {
         LimpaQuadro(36,10,18,1);
         return;
        }
      cc = 0;
      while( !Compara(palavra,(*def[cc]).nome) && (cc < cont_def) && cont_def )
        cc++;
      if ( (cc < cont_def) && cont_def )
        {
         EscrXY(48,11,(*def[cc]).nome);
         eiw = (*def[cc]).inter;
         sair = true;
        }
        else
        {
         Copia(palavra,Maiuscula(palavra));
         switch ( strlen(palavra) )
           {
            case 3 : Copia(bufftxt,"00");
                     Concat(bufftxt,Extrai(palavra,1,2));
                     break;
            case 4 : Copia(bufftxt,"0");
                     Concat(bufftxt,Extrai(palavra,1,3));
                     break;
           }
         eiw = HexDecW(bufftxt);
         if ( !error_conv && (eiw <= MAXINTERNO) )
           {
            if ( *palavra == 'R' )
              {
               eiw |= 0x0400;
               *bufftxt = *palavra;
              }
              else
              *bufftxt = VerifEISR(eiw);
            EscrXY(48,11,bufftxt);
            sair = true;
           }
        }
     } while( !sair );
   if ( eiw & 0x0400 )
     {
      Quadro(30,14,13,1,"Valor : xxxxx");
      do
        {
         Copia(palavra,StrW(reg_int[eiw & 0xFBFF]));
         LeString(40,15,5,palavra);
         vreg = Val(palavra);
        } while ( error_conv && (tecpc != _esc) );
        if ( tecpc != _esc )
          reg_int[eiw & 0xFBFF] = vreg;
     }
     else
     {
      Quadro(25,14,29,1,"Forca Aberto/Fechado ? (A/F)");
      LeTecla();
      tecpc = toupper(tecpc);
      if ( (tecpc == 'A') || (tecpc == 'F') )
        {
         if ( tecpc == 'A' )
           est_int[eiw] = 0x10;
           else
           est_int[eiw] = 0x11;
        }
     }
   ListMonitEI();
   LimpaQuadro(36,10,18,1);
   LimpaQuadro(25,14,29,1);
  }

void near ForcaEstMonit(void)
  {
   register int cc = 0,vreg;
   if ( !c_monit_ei )
     return;
   do
     {
      CorFundo(verde);
      CorTexto(marrom);
      EscrXY(66,6+cc,lab_monit_ei[cc]);
      LeTecla();
      CorFundo(cinza);
      CorTexto(azul);
      EscrXY(66,6+cc,lab_monit_ei[cc]);
      tecpc = toupper(tecpc);
      switch ( tecpc )
        {
         case _up    : if ( cc > 0 )
                         cc--;
                         else
                         cc = c_monit_ei - 1;
                       break;
         case _down  : if ( cc < c_monit_ei - 1 )
                         cc++;
                         else
                         cc = 0;
                       break;
         case 'F'    :
         case 'A'    :
         case 'N'    : if ( !(monit_ei[cc] & 0x0400) )
                         {
                          switch ( tecpc )
                            {
                             case 'F' : est_int[monit_ei[cc]] = 0x11;
                                        break;
                             case 'A' : est_int[monit_ei[cc]] = 0x10;
                                        break;
                             case 'N' : est_int[monit_ei[cc]] &= 0xEF;
                                        break;
                            }
                          ListMonitEI();
                         }
                       break;
         case _enter : if ( monit_ei[cc] & 0x0400 )
                         {
                          do
                            {
                             Copia(palavra,"-----");
                             LeString(74,6+cc,5,palavra);
                             vreg = Val(palavra);
                            } while( error_conv && (tecpc != _esc) );
                          if ( tecpc != _esc )
                            {
                             reg_int[monit_ei[cc] & 0xFBFF] = vreg;
                             ListMonitEI();
                            }
                            else
                            tecpc = 0;
                         }
                       break;
        }
     } while( tecpc != _esc );
  }

void near NormalEstInt(void)
  {
   register int cc;
   char cei,err,ei_ok,sair = false;
   int eiw;
   Quadro(36,10,18,1,"Est.Int.:         ");
   do
     {
      Copia(palavra,"        ");
      LeString(48,11,8,palavra);
      palavra[PosFimSpc(palavra)+1] = '\0';
      if ( tecpc != _enter )
        {
         LimpaQuadro(36,10,18,1);
         return;
        }
      cc = 0;
      while( !Compara(palavra,(*def[cc]).nome) && (cc < cont_def) && cont_def )
        cc++;
      if ( (cc < cont_def) && cont_def )
        {
         EscrXY(48,11,(*def[cc]).nome);
         eiw = (*def[cc]).inter;
         sair = true;
        }
        else
        {
         Copia(palavra,Maiuscula(palavra));
         switch ( strlen(palavra) )
           {
            case 3 : Copia(bufftxt,"00");
                     Concat(bufftxt,Extrai(palavra,1,2));
                     break;
            case 4 : Copia(bufftxt,"0");
                     Concat(bufftxt,Extrai(palavra,1,3));
                     break;
           }
         eiw = HexDecW(bufftxt);
         if ( !error_conv && (eiw <= MAXINTERNO) )
           {
            if ( *palavra == 'R' )
              eiw |= 0x0400;
              else
              *bufftxt = VerifEISR(eiw);
            EscrXY(48,11,bufftxt);
            sair = true;
           }
        }
     } while( !sair );
   if ( eiw <= MAXINTERNO )
     est_int[eiw] &= 0xEF;
   ListMonitEI();
   LimpaQuadro(36,10,18,1);
  }


void near DeletaEstInt(void)
  {
   register int cc = 0,ccc;
   if ( !c_monit_ei )
     return;
   do
     {
      CorFundo(verde);
      CorTexto(marrom);
      EscrXY(66,6+cc,lab_monit_ei[cc]);
      LeTecla();
      CorFundo(cinza);
      CorTexto(azul);
      EscrXY(66,6+cc,lab_monit_ei[cc]);
      switch ( tecpc )
        {
         case _up    : if ( cc > 0 )
                         cc--;
                         else
                         cc = c_monit_ei - 1;
                       break;
         case _down  : if ( cc < c_monit_ei - 1 )
                         cc++;
                         else
                         cc = 0;
                       break;
         case _enter : if ( cc < c_monit_ei - 1 )
                         {
                          for ( ccc = cc ; ccc < c_monit_ei-1 ; ccc++ )
                             {
                              monit_ei[ccc] = monit_ei[ccc+1];
                              Copia(lab_monit_ei[ccc],lab_monit_ei[ccc+1]);
                             }
                         }
                         else
                         if ( cc > 0 )
                           cc--;
                       c_monit_ei--;
                       ListMonitEI();
                       break;
        }
     } while( (tecpc != _esc) && c_monit_ei );
  }

void near EntraTempCont(void)
  {
   register int cc;
   char ctc,cmax;
   Quadro(36,10,18,1,"Temp./Cont.:      ");
   do
     {
      Copia(palavra,"  ");
      LeString(51,11,2,palavra);
      if ( tecpc != _enter )
        {
         LimpaQuadro(36,10,18,1);
         return;
        }
      Copia(palavra,Maiuscula(palavra));
      ctc = HexDec(palavra);
     } while ( (ctc > MAXTIMR) || error_conv );
   if ( temp[ctc].codt.bit.pre )
     {
      Quadro(25,14,16,1,"Preset :        ");
      cmax = ( temp[ctc].codt.bit.h/*tim*/ ) ? 5 : 4;
      do
        {
         Copia(palavra,DadoTemp(ctc,temp[ctc].pr));
         LeString(35,15,cmax,palavra);
         if ( tecpc == _enter )
           {
            cc = 0;
            while ( *(palavra+cc) && (*(palavra+cc) != '.') )
              cc++;
            if ( *(palavra+cc) == '.' )
              while( *(palavra+cc) )
                {
                 *(palavra+cc) = *(palavra+cc+1);
                 cc++;
                }
            temp[ctc].pr = Val(palavra);
           }
           else
           {
            LimpaQuadro(36,10,18,1);
            LimpaQuadro(25,14,15,1);
            return;
           }
        } while( error_conv );
     }
   cc = 0;
   while ( (monit_temp[cc] != ctc) && (cc < cont_temp) )
     cc++;
   if ( cc == cont_temp )
     {
      if ( cont_temp < 9 )
        {
         monit_temp[cont_temp] = ctc;
         cont_temp++;
        }
        else
        {
         for ( cc = 0 ; cc < 8 ; cc++ )
           monit_temp[cc] = monit_temp[cc+1];
         monit_temp[cc] = ctc;
        }
     }
   ListMonitTemp();
   LimpaQuadro(36,10,18,1);
   LimpaQuadro(25,14,15,1);
  }

void near TelaHelp(void)
  {
   #define NUMHELP 12
   const char help[NUMHELP][41] =
     {
      "F1 - HELP.",
      "F2 - Forca Estado Interno (geral).",
      "F3 - Normaliza Estado Interno.    ",
      "F4 - Entra/Altera Tempo ou Contador.",
      "F5 - Deleta Tempo ou Contador.",
      "F10- Ativa/Desativa Simulacao.",
      "F  - Forca Est. Int. em monitoracao.",
      "Ins- Insere Est.Int. p/ monitoracao.",
      "Del- Deleta Est.Int. em monitoracao.",
      "Dir/Esq - Move Cursor em Entr.Digital.",
      "Cim/Baix- Alterna Cursor E.D. <-> S.D.",
      "PgUp/PgDown - Seleciona Placa E.D./S.D."
     };
   register int cc = 0, ccc;
   MoldQuadro(21,11,39,10);
   EscrXY(30,11,"HELP");
   while ( tecpc != _esc )
     {
      for ( ccc = 0 ; (cc+ccc < NUMHELP) && (ccc < 10) ; ccc++ )
	EscrXY(22,12+ccc,ComplVazio(help[ccc+cc],39));
      for (; ccc < 10 ; ccc++)
        EscrXY(22,12+ccc,ComplVazio(" ",39));
      LeTecla();
      if ( tecpc == _F1 )
        {
         if ( cc + 10 > NUMHELP )
           cc = 0;
           else
           cc += 10;
        }
     }
   LimpaQuadro(21,11,38,10);
  }

void near AtualizaBufferED(void)
  {
   register int cc,
                pbed = offed*16,
                ed = inic_ed + offed*16;
   char ced[2] = { 0,0 };
   for ( cc = 0 ; cc < 16 ; cc++ )
     {
      buffed[pbed+cc] = est_int[ed+cc];
      *ced = ( buffed[pbed+cc] & 0x01 ) ? lig : des;
      if ( buffed[pbed+cc] & 0x10 )
        {
         CorFundo(azul);
         CorTexto(amarelo);
        }
        else
        {
         CorFundo(cinza);
         CorTexto(azul);
        }
      EscrXY(3*cc+4,7,ced);
     }
  }

void near EscrCursorES( char np, char lig )
  {
   char psk[2] = { 0,0 };
   CorTexto(vermelho);
   CorFundo(cinza);
   PiscaON();
   *psk = ( lig ) ? '>' : ' ';
   EscrXY(3,4+3*np,psk);
   PiscaOFF();
  }

void near Simulacao(void)
  {
   char sai_sim = false,flag_sim = false,
        pos_ed = 0,ned = 0;
   char nplc[2],cfgd[2],
        cd = 1;
   nplc[0] = offsd + 1;
   nplc[1] = offed + 1;
   cfgd[0] = cfg_sd;
   cfgd[1] = cfg_ed;
   InicializaSimulacao();
   CorFundo(cinza);
   CorTexto(azul);
   MoldQuadro(2,3,56,5);
   EscrXY(29,3,"SAIDAS");
   EscrXY(2,6,lin_sim);
   EscrXY(60,25,"Simulacao : OFF ");
   EscrXY(40,25,"Varr.: ---.-- [ms]");
   MoldQuadro(64,3,12,18);
   EscrXY(65,4,"   Est. Int. ");
   ListMonitEI();
   MolduraMonitES();
   MostraConfig();
   MoldTemp();
   ListMonitTemp();
   EscrCursorES(cd,true);
   CorTexto(marrom);
   CorFundo(verde);
   EscrXY(3*pos_ed+4,8,DecHex(pos_ed));
   do
     {
      if ( flag_sim )
        sai_sim = AcionaSimulacao();
      if ( TeclaPress() )
        {
         LeTecla();
         switch ( tecpc )
           {
            case _F1    : TelaHelp();
                          break;
            case _F2    : ForcaEstInt();
                          AtualizaBufferED();
                          break;
            case _F3    : NormalEstInt();
                          AtualizaBufferED();
                          break;
            case _F4    : EntraTempCont();
                          break;
            case _F5    : DeletaMonitTemp();
                          break;
            case _F10   : flag_sim ^= 0x01;
                          if ( flag_sim )
                            EscrXY(72,25,"ON ");
                            else
                            {
                             EscrXY(72,25,"OFF");
                             est_int[ONVARR1] = 0x11;
                            }
                          break;
            case _ins   : InsereEstInt();
                          break;
            case _del   : DeletaEstInt();
                          break;

            case _right :
            case _left  : CorFundo(cinza);
                          CorTexto(azul);
                          EscrXY(3*pos_ed+4,8,DecHex(ned));
                          switch (tecpc)
                            {
                             case _right : if ( pos_ed < 15 )
                                             pos_ed++;
                                             else
                                             pos_ed = 0;
                                           break;
                             case _left  : if ( pos_ed > 0 )
                                             pos_ed--;
                                             else
                                             pos_ed = 15;
                                           break;
                            }
                          ned = inic_ed + pos_ed + (nplc[1]-1)*16;
                          break;
            case _pgdown :
            case _pgup  : switch ( tecpc )
                            {
                             case _pgdown : if ( nplc[cd] < cfgd[cd] )
                                              nplc[cd]++;
                                              else
                                              nplc[cd] = 1;
                                            break;
                             case _pgup   : if ( nplc[cd] > 1 )
                                              nplc[cd]--;
                                              else
                                              nplc[cd] = cfgd[cd];
                                            break;
                            }
                          ned = inic_ed + pos_ed + (nplc[1]-1)*16;
                          offsd = nplc[0] - 1;
                          offed = nplc[1] - 1;
                          MolduraMonitES();
                          AtualizaBufferED();
                          break;
            case _up    :
            case _down  : EscrCursorES(cd,false);
                          cd = ( cd ) ? 0 : 1;
                          EscrCursorES(cd,true);
                          break;
            case 'F'    :
            case 'f'    : ForcaEstMonit();
                          break;
            case _esc   : sai_sim = true;
                          break;
            case _enter : if ( !(buffed[offed*16+pos_ed] & 0x10) )
                            {
                             char ced[2] = { 0,0 };
                             CorTexto(azul);
                             CorFundo(cinza);
                             buffed[offed*16+pos_ed] ^= 0x01;
                             if ( buffed[offed*16+pos_ed] )
                               *ced = lig;
                               else
                               *ced = des;
                             EscrXY(3*pos_ed+4,7,ced);
                            }
                          break;
           }
         CorTexto(marrom);
         CorFundo(verde);
         EscrXY(3*pos_ed+4,8,DecHex(ned));
        }
     } while( !sai_sim );
   if ( cont_temp || c_monit_ei )
     acion_sim = true;
     else
     acion_sim = false;
  }

void main(void)
  {
/*
   ncom = 1;
   nclp = 1;
   InstalaTimer();
   ConfigPortCom();
   InicializaCom();

*/
//   Inicializa_Serial(COM0,TAXA,PARIDADE,STOP,BITS_DADOS);



   PiscaOFF();
   CursorOFF();
   CorFundo(azul);
   CorTexto(amarelo);
   LimpaTela();
   Quadro(26,10,24,3,MENSINIC);
   LeTecla();
   LimpaQuadro(26,10,24,3);
   InicializaHeap();
   Inicializacao();
   InicializaBuffer();
   LimpaBuffer();
   VerifArqCFG();
   VerificaCfgProg();
   CursorOFF();
   flag_error = false;
   Moldura();
   VerificaHeap(0);
   do
     {
      EscrNomeArq();
      EscrStatusMem();
      if ( ( flag_editerr) )
        {
         esc_princ = 1;
         flag_editerr = false;
        }
	else
        MenuPrinc();
      switch (esc_princ)
        {
         case 0 : Arquivos();
                  break;
         case 1 : switch ( esc_edit )
                    {
                     case 0 : if ( !flag_ladder)
                                EditProg();
                              break;
		 case 1 : EditTelas();
                              break;
                    }
                  break;
         case 2 : switch (esc_comp)
                    {
                     case 0 : if ( !flag_ladder )
                                Compilar();
                              break;
		     case 1 : ReferCruzada();
			      break;
		     case 2 : EditaLadder();
			      break;
		    }
		  break;
	 case 3 : EditBuffer();
		  break;
	 case 4 : Simulacao();
		  Moldura();
		  break;
	 case 5 : switch (esc_com)
		    {
		     case 0 : ///Transmitir();     /*EscrDadosCLP();*/
			      break;
		     case 1 : ////Leitura();     /*LerDadosCLP();*/
			      break;
		    }
	}
     } while(!sair);
   SalvarArqCFG();
   if ( acion_sim )
     SalvarArqSim();
   CursorON();
   CorFundo(preto);
   CorTexto(branco);
   LimpaTela();
   GotoXY(1,1);
/*
   RestauraCom();
   RecuperaTimer();
*/
  }

void Transmitir(void)
{
 unsigned char ch;
 int status;
 do
 {
   if(kbhit())
   {
      ch = getch();
      outportb(COM0,ch);
   }
 }while(ch!= 0x1b);
}

/*****************************************
*    RecebeArq()			 *
*    Recebe um Arquivo pela porta serial *
*****************************************/

/*

void Recebe_Arq(char *nomearq)
{
  int status;
  FILE *f;
  unsigned char ch=0;
  union{
    unsigned long arq;
    unsigned char byte[4];
  }tamanho;
  unsigned long i=0;
  if((f=fopen(nomearq,"wb")) == NULL)
  {
    printf("\nNao Posso Abrir arquivo &s",nomearq);
    exit(1);
  }
  // Inicializa UART
  Inicializa_Serial(COM0,TAXA,PARIDADE,STOP,BITS_DADOS);
  while(TRUE)
  {
    status=inport(COM0+5);
    if((status & 1))		// dados disponiveis ?
    {
      tamanho.byte[i++]=inportb(COM0);
      outportb(COM0,'*');
      if(i==4)break;
    }
    i=0L;
    while(i<tamanho.arq)
    {
      status=inport(COM0+5);
      if((status & 1))          // Dados Disponiveis ?
      {
	ch=inportb(COM0);       // Recebe Caractere
	putc(ch,f);      	// Grava no Arquivo
	outportb(COM0,'*');
	i++;
      }
    }
    fclose(f);
  }
}

*/
/************************************************
* EnviaArq()					*
* Envia um arquivo pela Porta Serial            *
************************************************/
/*
void Envia_Arq(char *nomearq)
{
   FILE *f;
   unsigned int ch;
   int status,i=0;
   union{
     unsigned long arq;
     unsigned char byte[4];
   }tamanho;
   if((f=fopen(nomearq,"rb")) == NULL)
   {
     printf("\nNao Posso abrir arquivo %s",nomearq);
     exit(1);
   }
   fseek(f,0L,2);
   tamanho.arq = ftell(f);
   fseek(f,0L,0);
   Inicializa_Serial(COM0,TAXA,PARIDADE,STOP,BITS_DADOS);
   for(i=0;i<4;i++)
   {
      outportb(COM0,tamanho.byte[i]);
      status=inport(COM0+5);
      while(!(status&1))
	status=inport(COM0+5);
      ch=inportb(COM0);
      if(ch!='*')
      {
	printf("\nErro na Comunica��o");
	exit(1);
      }
   }
   while((ch=getc(f))!=(unsigned int)EOF)
   {
     outportb(COM0,ch);
     status=inport(COM0+5);
     while(!(status&1))
      status=inport(COM0+5);
     ch=inportb(COM0);
     if(ch!='*')
     {
       printf("\nErro na Comunica��o");
       exit(1);
     }
   }
   fclose(f);
   exit(1);
}
*/

