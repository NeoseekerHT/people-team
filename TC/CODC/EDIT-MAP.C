/*
		EDIT-MAP.C

	Rotinas de Edicao da Tabela de Mapas
*/


#include       <alloc.h>
#include       <mem.h>
#include       <dos.h>
#include       <graphics.h>
#include       <string.h>

#include       <stdio.h>
#include       <conio.h>


#include       "config.h"
#include       "estrut.h"
#include       "globais.h"



void Edita_Mapa( union u_simbolo *, int, int );
void Salva_Imagem( char *, char, char, char, char );
void Edita_Quadro( void );
void Edita_Menus( char );

#define          VIDEO               0xA000
#define          VIDEO_PAR           0xBC00
#define          VIDEO_IMPAR         0xBE00

extern int       G_Driver, G_Mode;




void Edita_Mapa( p_mapa, xi, yi )
union u_simbolo   *p_mapa;
int                xi, yi;
{

   char            *pont, *p_video, *mapa, *video, *video_par, *video_impar;

   int              tx, ty, inicio, comeco, fim;
   register int     y;


   tx = ( p_mapa->dimensao.coluna + 1 ) / 8;
   ty = p_mapa->dimensao.linha + 1;
   if ( G_Mode == 6 )
   {

      mapa = ( char far * )++p_mapa;
      video_par = ( char far * )MK_FP( VIDEO_PAR, 0 );
      video_impar = ( char far * )MK_FP( VIDEO_IMPAR, 0 );
      inicio = yi + yi % 2;
      fim = yi + ty + ( yi + ty ) % 2;
      if ( fim > 0 )
      {
         if ( fim > 200 )
            fim = 200;

         if ( inicio > 0 )
            comeco = inicio;
         else
             comeco = 0;
         for ( y = comeco; y < fim; y += 2 )
         {
             p_video = video_par + y * 40 + xi;
	     pont = mapa + ( y - yi ) * tx;
	     movmem( pont, p_video, tx );
         }

      }
      inicio = yi + ( yi + 1 ) % 2;
      fim = yi + ty + ( yi + ty + 1 ) % 2;
      if ( fim > 0 )
      {
         if ( fim > 200 )
            fim = 200;

         if ( inicio > 0 )
            comeco = inicio;
         else
             comeco = 1;
         for ( y = comeco; y < fim; y += 2 )
         {
             p_video = video_impar + ( y - 1 ) * 40 + xi;
             pont = mapa + ( y - yi ) * tx;
	     movmem( pont, p_video, tx );
         }
      }

   }
   else
       if ( G_Mode == 14 )
       {

          mapa = ( char far * )++p_mapa;
          video = ( char far * )MK_FP( VIDEO, 0 );
          inicio = yi;
          fim = yi + ty;
          if ( fim > 0 )
          {
             if ( fim > 200 )
                fim = 200;

             if ( inicio > 0 )
                comeco = inicio;
             else
                 comeco = 0;
             for ( y = comeco; y < fim; y++ )
             {
                 p_video = video + y * 80 +  xi;
                 pont = mapa + ( y - yi ) * tx;
                 movmem( pont, p_video, tx );
             }

          }

       }


}                     /*  fim de Edita_Mapa  */



void Salva_Imagem( mapa, xi, yi, xf, yf )
char	*mapa, xi, yi, xf, yf;
{

   char      *video, *video_par, *video_impar,
              i, largura;


   largura = xf - xi + 1;
   if ( G_Mode == 6 )
   {

      video_par = MK_FP( VIDEO_PAR, 0 );
      video_impar = MK_FP( VIDEO_IMPAR, 0 );

      for ( i = yi; i < yf; i += 2 )
          movmem( video_par + i * 40 + xi, mapa + ( i - yi ) * largura, largura );

      for ( i = yi + 1; i <= yf; i += 2 )
          movmem( video_impar + ( i - 1 ) * 40 + xi, mapa + ( i - yi ) * largura, largura );

   }
   else
       if ( G_Mode == 14 )
       {

          video = MK_FP( VIDEO, 0 );

          for ( i = yi; i <= yf; i++ )
              movmem( video + i * 80 + xi, mapa + ( i - yi ) * largura, largura );

       }


}                       /*  fim de Salva Imagem  */




void Edita_Quadro( void )
{

     union u_simbolo    *quadro;

     int                 l, col = 80, lin = 200;

     char               *m, descricao[25] = { "Descri, c~ ao  da  Linha\0" };



     descricao[7] = 8;
     descricao[10] = 8;

     quadro = ( union u_simbolo * )calloc( 4 + col * lin, 1 );
     quadro->dimensao.coluna = col * 8 - 1;
     quadro->dimensao.linha = lin - 1;
     m = ( char * ) ( quadro + 1 );

     setmem( m, 80, 255 );

     for ( l = 1; l < 3; l++ )
     {
         *( m + l * 80 ) = 192;
         *( m + l * 80 + 79 ) = 3;
     }

     for ( l = 3; l < 24; l++ )
     {
         *( m + l * 80 ) = 195;
         *( m + l * 80 +  8 ) = 24;
         *( m + l * 80 + 17 ) = 24;
         *( m + l * 80 + 66 ) = 24;
         *( m + l * 80 + 79 ) = 195;
     }

     setmem( m +  3 * 80 + 1, 78, 255 );
     setmem( m + 13 * 80 + 1, 78, 255 );

     *( m + 24 * 80 ) = 195;
     setmem( m + 24 * 80 + 1, 78, 255 );
     *( m + 24 * 80 + 79 ) = 195;

     for ( l = 25; l < 27; l++ )
     {
         *( m + l * 80 ) = 192;
         *( m + l * 80 + 79 ) = 3;
     }

     *( m + 27 * 80 ) = 195;
     setmem( m + 27 * 80 + 1, 78, 255 );
     *( m + 27 * 80 + 79 ) = 195;

     for ( l = 28; l < 172; l++ )
     {
         *( m + l * 80 ) = 195;
         *( m + l * 80 + 76 ) = 192;
         *( m + l * 80 + 79 ) = 195;
     }

     *( m + 172 * 80 ) = 195;
     setmem( m + 172 * 80 + 1, 78, 255 );
     *( m + 172 * 80 + 79 ) = 195;

     for ( l = 173; l < 175; l++ )
     {
         *( m + l * 80 ) = 192;
         *( m + l * 80 + 79 ) = 3;
     }

     setmem( m + 175 * 80, 80, 255 );

     Escreve_na_Janela( "Linha\0",    quadro,  16, 5 );
     Escreve_na_Janela( "Nome\0",     quadro,  88, 5 );
     Escreve_na_Janela( descricao,    quadro, 256, 5 );
     Escreve_na_Janela( "Programa\0", quadro, 552, 5 );

     Edita_Mapa( quadro, 0, 0 );


}                       /*  fim de Edita Quadro  */




void Edita_Menus( char   menu )
{

     struct e_opcao      *opcao;
     union  u_simbolo    *mapa;



     mapa = calloc( 4 + NUM_LINHAS_MENU * 8 * 80, 1 );
     mapa->dimensao.coluna = 80 * 8 - 1;
     mapa->dimensao.linha = 8 * NUM_LINHAS_MENU - 1;

     if ( menu < TAM_t_menus )
        opcao = *( t_menus + menu );
     else
         opcao = 0;

     while ( opcao )
     {
           if ( opcao->linha < NUM_LINHAS_MENU )
	      Escreve_na_Janela( opcao->funcao, mapa, opcao->coluna * 8, opcao->linha * 8 );
           opcao = opcao->prox_opcao;
     }

     Edita_Mapa( mapa, MENU_COLUNA, MENU_LINHA );

     free( mapa );


}                       /*  fim de Edita Menu  */
