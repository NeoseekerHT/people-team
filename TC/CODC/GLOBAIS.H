/*
                GLOBAIS.H

        Cabecalho de definicao da variaveis globais
*/



extern  struct e_linha                  *t_linhas;

extern  struct e_bloco                  *t_blocos;

extern  struct e_bloco                  *t_blocos_aux;

extern  struct e_grupo                  *t_grupos;

extern  struct e_contatos               *t_contatos;

extern  struct e_no                     *t_nos;

extern  struct e_sub_no                 *t_sub_nos;

extern  struct e_mapa_contatos          *mapa_contatos;

extern  struct e_cursor                  cursor;

extern  struct e_marca                   marca;

extern  struct e_pos_cont                novo_contato;

extern  struct e_tabela_validade        *t_tabela_validade;

extern  struct e_janela                 *t_janelas;

extern  struct e_tabela_de_indices      *tabela_de_classes;

extern  struct e_nome_arq                nome_arq;

extern  struct e_opcao                 **t_menus;

extern  struct e_caracter                p_sai[16];

extern  struct e_blc_end                 t_blc_end[90];

extern  union  u_simbolo               **t_simbolos,
                                        *mp_bits;

extern  union  u_ponto                  *t_pontos;


extern  int                              num_linhas,
                                         num_blocos,
                                         TAM_t_linhas,
                                         TAM_t_simbolos,
                                         TAM_t_menus,
                                         TAM_t_blocos,
					 TAM_t_grupos,
					 TAM_t_pontos,
					 TAM_t_contatos,
					 TAM_t_nos,
					 TAM_t_sub_nos,
                                         tamanho_tabela_de_classes,
                                         linha_corrente,
                                         desl_temp,
                                         reg_temp,
 				         End_Codigo,
 				         End_Tab_Leds,
 				         End_Bits,
				         End_Val_Temp,
				         End_Val_Selet,
				         End_Val_E_An,
				         End_Val_S_An,
				         End_Prs_Temp,
				         End_Prs_E_An,
				         End_Prs_S_An;



extern  char                            *mp_pontos,
                                        *mp_pontos_aux,
                                        *p_pilhas,
					 p_ramo[128],
					 Tabela_Leds[48],
                                         pilha,
					 pilha_sai,
					 pilha_ramo,
					 ultimo_grupo,
					 monitorando,
					 diag_alt,
					 temp_iniciado,
                                         u_contato,
                                         u_no,
                                         u_sub_no,
                                         u_ponto,
                                         u_ramo,
                                         cor_byte_ativo,
                                         cor_byte_nao_ativo,
 			                 cor_inicio_ativo,
			                 cor_inicio_n_ativo,
			                 cor_fim_ativo,
			                 cor_fim_nao_ativo,
			                 cor_r_inic_ativo,
			                 cor_r_inic_n_ativo,
			                 cor_r_fim_ativo,
			                 cor_r_fim_nao_ativo,
					 status_temp,
					 Porta_Serial,
				         Taxa_Transmissao,
				         Paridade,
				         Stop_Bits,
   				         Num_Bits,
					 Num_Estacao,
					 num_nos,
					 nos_iniciais[32],
                                         matriz_car[128][8],
                                         matriz_chaves[64],
                                         matriz_chaves_parcial[64],
					 dir_arq[256],
                                         diretorio[240],
                                         drive[2],
					 fim_drv[2],
                                         ext_diag[4],
                                         ext_leds[4],
                                         ext_atrb[4],
                                         ext_list[4],
                                         ext_dtemp[4],
                                         ext_ltemp[4],
                                         ext_atemp[4],
                                         CURSOR[12],
                                         MARCA_[12];
