/*
		RASS-SAI.C

        Rotinas de complementacao das Associacoes Serie e Paralelo

        Rotina de Saida
*/


#include  <alloc.h>
#include  <stdlib.h>


#include  "config.h"
#include  "estrut.h"
#include  "globais.h"


void  Troca_Ramo_Serie( struct e_grupo *, char, struct e_linha * );
void  Troca_Ramo_Paralelo( struct e_grupo *, char, struct e_linha * );
void  Troca_No( char, char, struct e_linha * );
void  Deslocamento_de_Contatos( signed char, signed char, char );
void  Deslocamento_de_Nos( signed char, signed char, char );
char  Monta_Tabela_de_Grupos( char, struct e_grupo *, struct e_grupo **, struct e_linha * );
void  Saida( char, struct e_linha *, char );
char  Complementa_Nos_do_Grupo( char, struct e_linha * );




void Troca_Ramo_Serie( grp, ramo, linha )
struct e_linha   *linha;
struct e_grupo   *grp;
char              ramo;

{

   struct e_grupo  *grp_1, *grp_2;


   grp->ramo = ramo;
   grp_1 = t_grupos + grp->grp_1;
   grp_1->ramo = ramo;
   if ( ( grp_1->status ) && ( grp_1->status < 5 ) )
      Troca_Ramo_Serie( grp_1, ramo, linha );

   grp_2 = t_grupos + grp->grp_2;
   grp_2->ramo = ramo;
   if ( ( grp_2->status ) && ( grp_2->status < 5 ) )
      Troca_Ramo_Serie( grp_2, ramo, linha );

}                  /*  fim de Troca_Ramo_Serie  */





void Troca_Ramo_Paralelo( grp, ramo, linha )
struct e_linha  *linha;
struct e_grupo  *grp;
char             ramo;

{

   struct e_grupo  *grp_1, *grp_2;


   grp->ramo = ramo;
   grp_1 = t_grupos + grp->grp_1;
   if ( grp_1->status > 4 )
      Troca_Ramo_Paralelo( grp_1, ramo, linha );

   grp_2 = t_grupos + grp->grp_2;
   if ( grp_2->status > 4 )
      Troca_Ramo_Paralelo( grp_2, ramo, linha );

}                    /*  fim de Troca_Ramo_Paralelo  */




void Troca_No( pto_orig, pto_dest, linha )
struct e_linha   *linha;
char              pto_orig, pto_dest;

{

   union u_ponto           *pto;

   struct e_grupo          *grupo;
   struct e_no             *l_no_ant;
   struct e_sub_no         *l_no_orig;

   char                    *l_grupo_orig, *l_grupo_dest,
                            no_orig, no_dest, l_no, l_no_dest, p;


   grupo = t_grupos + linha->grupo;
   grupo = t_grupos + grupo->grp_2;

   pto = t_pontos + pto_orig;
   no_orig = pto->no_grupo.no;
   l_grupo_orig = ( t_nos + no_orig )->grupo;
   l_no_orig = t_sub_nos + ( t_nos + no_orig )->sub_no;
   while ( l_no_orig->prox_sub_no )
         l_no_orig = t_sub_nos + l_no_orig->prox_sub_no;

   pto = t_pontos + pto_dest;
   no_dest = pto->no_grupo.no;
   for ( p = 0; p < ( t_nos + no_orig )->num_grupos; p++ )
       Introduz_Grupo_no_No( *( l_grupo_orig + p ), no_dest, linha );

   l_no_dest = ( t_nos + no_dest )->sub_no;
   l_no_orig->prox_sub_no = l_no_dest;
   ( t_nos + no_dest )->sub_no = ( t_nos + no_orig )->sub_no;

   l_no = grupo->no;
   if ( l_no == no_orig )
      grupo->no = ( t_nos + l_no )->prox_no;
   else
   {
       while ( l_no != no_orig )
       {
             l_no_ant = t_nos + l_no;
             l_no = ( t_nos + l_no )->prox_no;
       }
       l_no_ant->prox_no = ( t_nos + l_no )->prox_no;
   }

   for ( p = pto_dest; p <= u_ponto; p++ )
   {
       pto = t_pontos + p;
       if ( pto->no_grupo.no == no_orig )
          pto->no_grupo.no = no_dest;
   }

}                    /*  fim de Troca_No  */




void Deslocamento_de_Contatos( desl_horz, desl_vert, lista_contatos )

signed char               desl_horz, desl_vert;
char                      lista_contatos;

{

   struct e_contatos  *contato;


   if ( desl_horz < 0 )
      desl_horz = 0;
   if ( desl_vert < 0 )
      desl_vert = 0;

   while ( lista_contatos )
   {
         contato = t_contatos + lista_contatos;
	 contato->xr_i += desl_horz;
         contato->xr_f += desl_horz;
         contato->xc_i += desl_horz;
         contato->xc_f += desl_horz;
         contato->yr += desl_vert;

         lista_contatos = contato->prox_contato;
   }

}                    /*  fim de Deslocamento_de_Contatos  */




void Deslocamento_de_Nos( desl_horz, desl_vert, lista_nos )

signed char          desl_horz, desl_vert;
char                 lista_nos;

{

   struct e_no      *no;
   struct e_sub_no  *sub_no;

   char              i;


   if ( desl_horz < 0 )
      desl_horz = 0;
   if ( desl_vert < 0 )
      desl_vert = 0;

   while ( lista_nos )
   {

         no = t_nos + lista_nos;
         sub_no = t_sub_nos + no->sub_no;
         while ( sub_no != t_sub_nos )
         {
               sub_no->xc += desl_horz;
	       sub_no->xl += desl_horz;
               sub_no->yi += desl_vert;
               sub_no->ym += desl_vert;
               sub_no->yf += desl_vert;

               for ( i = 0; i < sub_no->num_pontos; i++ )
                   ( sub_no->lista_de_pontos + i )->linha += desl_vert;

               sub_no = t_sub_nos + sub_no->prox_sub_no;
         }
         lista_nos = no->prox_no;

   }

}                  /*  fim de Deslocamento_de_Nos  */





char  Monta_Tabela_de_Grupos( g, grup, p_tab, linha )
struct e_linha  *linha;
struct e_grupo  *grup, **p_tab;
char             g;
{

   struct e_grupo  *grp_1, *grp_2;


   grp_2 = t_grupos + grup->grp_2;
   if ( grp_2->status > 4 )
   {
      *( p_tab + g++ ) = grp_2;
      g = Monta_Tabela_de_Grupos( g, grp_2, p_tab, linha );
   }

   grp_1 = t_grupos + grup->grp_1;
   if ( grp_1->status > 4 )
   {
      *( p_tab + g++ ) = grp_1;
      g = Monta_Tabela_de_Grupos( g, grp_1, p_tab, linha );
   }

   return g;

}                   /*  fim de Monta_Tabela_de_Grupos  */




void Saida( endereco, linha, pos_linha )
struct e_linha   *linha;
char              endereco, pos_linha;
{


   union u_simbolo           *simbolo;
   union u_ponto             *pto;

   struct e_componente       *componente;
   struct e_contatos         *saida;
   struct e_grupo            *grupo, *grupo_ant;
   struct e_bloco            *bloco;

   char                      desl_horz  = 0,
                             num_de_colunas, num_de_linhas;


   bloco = t_blocos + linha->codigo + endereco;
   componente = tabela_de_classes->t_componentes + bloco->contato;
   simbolo = *( t_simbolos + componente->n_saida );
   num_de_colunas = ( simbolo->dimensao.coluna + 1 ) / 8;
   num_de_linhas  = ( simbolo->dimensao.linha + 1 ) / 8;

   saida = t_contatos + ++u_contato;
   if ( bloco == novo_contato.bloco  &&  novo_contato.status  &&  novo_contato.status < 3 )
      novo_contato.contato = saida;

   grupo = t_grupos + linha->grupo - 1;
   grupo->grp_2 = linha->grupo - 1;
   grupo->contato = u_contato;
   grupo->no = 0;
   grupo->num_de_colunas = TAM_MAX_LINHA;

   u_ponto++;
   if ( pilha )
   {
      grupo_ant = t_grupos + *( p_pilhas + pilha );
      if ( grupo_ant->pto_inicio )
         grupo->pto_inicio = grupo_ant->pto_inicio;
      else
          grupo->pto_inicio = u_ponto;
      grupo->grp_1 = *( p_pilhas + pilha );
      if ( grupo_ant->num_de_linhas > num_de_linhas + 1 )
      {
         grupo->num_de_linhas = grupo_ant->num_de_linhas;
         grupo->deslocamento = grupo_ant->deslocamento;
      }
      else
          grupo->num_de_linhas = num_de_linhas + 1;
      switch( ( t_blocos + linha->codigo + grupo_ant->end_inicio )->operacao )
      {

            case CNT_C :
            case CNT_D :
            case CNT_c :
            case CNT_d :
            case SELET :
            case ENT_A : if ( grupo_ant->status )
                         {
/*
                            desl_horz = ( TAM_MAX_LINHA - num_de_colunas - 2 - grupo_ant->num_de_colunas ) / 2;
                            Deslocamento_de_Contatos( desl_horz, 0, grupo_ant->contato );
                            Deslocamento_de_Nos( desl_horz, 0, grupo_ant->no );
*/
                            saida->prox_contato = grupo_ant->contato;
			    grupo_ant->contato = 0;
                            grupo->no = grupo_ant->no;
			    grupo_ant->no = 0;
                         }
                         break;

            default    : if ( grupo_ant->contato )
	                 {
/*
			    desl_horz = ( TAM_MAX_LINHA - num_de_colunas - 2 - grupo_ant->num_de_colunas ) / 2;
                            Deslocamento_de_Contatos( desl_horz, 0, grupo_ant->contato );
                            Deslocamento_de_Nos( desl_horz, 0, grupo_ant->no );
*/
                            saida->prox_contato = grupo_ant->contato;
			    grupo_ant->contato = 0;
                            grupo->no = grupo_ant->no;
			    grupo_ant->no = 0;
			 }

      }
      desl_horz += grupo_ant->num_de_colunas;
   }
   else
   {
       grupo->num_de_linhas = num_de_linhas + 1;
       grupo->pto_inicio = u_ponto;
   }

   saida->contato      = bloco->contato;
   saida->tipo         = SAIDA;
   saida->fase         = 0;
   if ( pilha )
      saida->no_ant = Complementa_Nos_do_Grupo( *( p_pilhas + pilha-- ), linha );
   else
   {
       saida->no_ant = ++u_no;
       Introduz_Grupo_no_No( linha->grupo - 1, u_no, linha );
   }
   saida->yr           = pos_linha;
   saida->xr_i         = desl_horz;
   saida->xr_f         = grupo->num_de_colunas - 1;
   saida->xc_i         = saida->xr_f - num_de_colunas;
   saida->xc_f         = saida->xr_f - 1;
   saida->pto_inicio   = u_ponto;
   saida->pto_contato  = ++u_ponto;

   pto = t_pontos + u_ponto - 1;
   pto->no_grupo.sinal = 1;
   pto->no_grupo.grupo = linha->grupo - 1;
   pto->no_grupo.no = saida->no_ant;
   pto = t_pontos + u_ponto;
   pto->contato = u_contato;

   grupo->pto_fim = ++u_ponto;

   pto = t_pontos + u_ponto;
   pto->no_grupo.sinal = 1;
   pto->no_grupo.grupo = linha->grupo - 1;


}                  /*  fim de Saida  */




char Complementa_Nos_do_Grupo( grup, linha )
struct e_linha  *linha;
char      grup;
{

   struct e_bloco          *bloco;
   struct e_grupo          *grupo;
   struct e_contatos       *contato;
   struct e_no             *no;
   struct e_sub_no         *sub_no;

   union  u_ponto          *pto;

   char                     num_colunas, ponto, status;



   grupo = t_grupos + grup;
   bloco = ( t_blocos + linha->codigo + grupo->end_inicio );
   switch( bloco->operacao )
   {

         case CNT_C :
         case CNT_D :
         case CNT_c :
         case CNT_d :
         case SELET :
         case ENT_A : if ( grupo->status )
                      {
                         while ( ( t_grupos + grupo->grp_1 )->status )
                               grupo = t_grupos + grupo->grp_1;
                         num_colunas = ( t_grupos + grupo->grp_1 )->num_de_colunas;
                         grupo = t_grupos + grupo->grp_2;
                         pto = t_pontos + grupo->pto_inicio;
                         no = t_nos + pto->no_grupo.no;
                         if ( no->sub_no )
                         {
                            sub_no = t_sub_nos + no->sub_no;
			    while ( sub_no->prox_sub_no )
			          sub_no = t_sub_nos + sub_no->prox_sub_no;
                            sub_no->xl = num_colunas;
                         }
                         else
                         {
                             contato = t_contatos + ( pto + 1 )->contato;
                             contato->xr_i = num_colunas;
                         }

                      }
                      break;

         default    : ponto = grupo->pto_inicio;
                      if ( ponto )
                      {

                         pto = t_pontos + ponto;
                         if ( !pto->no_grupo.no )
                         {
                            pto->no_grupo.no = ++u_no;
                            contato = t_contatos + ( pto + 1 )->contato;
                            contato->xr_i = 0;
                         }
                         else
                         {
                             no = t_nos + pto->no_grupo.no;
                             if ( no->sub_no )
                             {
                                sub_no = t_sub_nos + no->sub_no;
			        while ( sub_no->prox_sub_no )
			              sub_no = t_sub_nos + sub_no->prox_sub_no;
                                if ( !sub_no->status  &&  !pilha_sai )
                                   sub_no->xl = 0;
				else
                                    switch ( grupo->status )
                                    {

                                        case 1 :
                                        case 2 :
                                        case 3 :
                                        case 4 : if ( !( pto + 1 )->no_grupo.sinal )
					         {
					            contato = t_contatos + ( pto + 1 )->contato;
                                                    grupo = t_grupos + ( t_grupos + *no->grupo )->grp_1;
                                                    contato->xr_i = grupo->num_de_colunas;
                                                    break;
                 			         }
		 		                 if ( pilha_sai )
				                 {
                                                    while ( grupo->status )
   			   	                          grupo = t_grupos + grupo->grp_1;
				                    sub_no = t_sub_nos + ( t_nos + pto->no_grupo.no )->sub_no;
                                                    sub_no->xl = grupo->num_de_colunas;
					            break;
				                 }
                                        case 5 :
                                        case 6 : grupo = t_grupos + pto->no_grupo.grupo;
                                                 bloco = t_blocos + linha->codigo + grupo->end_inicio;
                                                 if ( !grupo->status )
					            switch ( bloco->operacao )
                                                    {
                                                       
					               case R_EMP :
					               case CNT_C :
						       case CNT_D :
						       case CNT_c :
						       case CNT_d : sub_no = t_sub_nos + ( t_nos + pto->no_grupo.no )->sub_no;
                                                                    sub_no->xl = grupo->num_de_colunas;
								    break;
									    
						       default    : contato = t_contatos + ( pto + 1 )->contato;
                                                                    grupo = t_grupos + ( t_grupos + *no->grupo )->grp_1;
                                                                    contato->xr_i = grupo->num_de_colunas;
									    
						    }
						 else
						     if ( grupo->status > 4 )
						     {
						        no = t_nos + pto->no_grupo.no;
						        grupo = t_grupos + ( t_grupos + *no->grupo )->grp_1;
							sub_no = t_sub_nos + ( t_nos + pto->no_grupo.no )->sub_no;
                                                        sub_no->xl = grupo->num_de_colunas;
						     }

                                    }
                             }
                             else
                                 if ( grupo->status )
                                 {
                                    contato = t_contatos + ( pto + 1 )->contato;
                                    grupo = t_grupos + ( t_grupos + *no->grupo )->grp_1;
                                    contato->xr_i = grupo->num_de_colunas;
                                 }
                         }

                         Introduz_Grupo_no_No( pto->no_grupo.grupo, pto->no_grupo.no, linha );
                         no = t_nos + pto->no_grupo.no;
			 status = ( t_grupos + pto->no_grupo.grupo )->status;
                         if ( *no->grupo != pto->no_grupo.grupo  &&  status  )
                         {
                            movmem( no->grupo, no->grupo + 1, no->num_grupos );
			    no->num_grupos++;
                            *no->grupo = pto->no_grupo.grupo;
                         }
		      
                         grupo = t_grupos + grup;
                         if ( grupo->status > 2 )
			    if ( grupo->status < 5 )
                               while ( grupo->status < 5 )
                                     grupo = t_grupos + grupo->grp_1;
			    else
  			        if ( grupo->grp_1 == grupo->grp_2 )
			           grupo = t_grupos + grupo->grp_1;
                         if ( grupo->status > 4 )
                            while ( grupo->status > 4  &&  ( t_grupos + grupo->grp_1 )->pto_inicio == ponto )
                            {
                                  pto = t_pontos + ( t_grupos + grupo->grp_2 )->pto_inicio;
                                  if ( !pto->no_grupo.no )
                                  {
                                     pto->no_grupo.no = ++u_no;
                                     contato = t_contatos + ( pto + 1 )->contato;
                                     contato->xr_i = 0;
                                  }
                                  else
                                  {
                                      no = t_nos + pto->no_grupo.no;
                                      if ( no->sub_no )
                                      {
                                         sub_no = t_sub_nos + no->sub_no;
                                         while ( sub_no->prox_sub_no )
                                               sub_no = t_sub_nos + sub_no->prox_sub_no;
                                         if ( !sub_no->status )
                                            sub_no->xl = 0;
                                      }
                                  }

                                  Introduz_Grupo_no_No( pto->no_grupo.grupo, pto->no_grupo.no, linha );
                                  no = t_nos + pto->no_grupo.no;
				  status = ( t_grupos + pto->no_grupo.grupo )->status;
                                  if ( *no->grupo != pto->no_grupo.grupo  &&  status  &&  status < 5 )
                                  {
                                     movmem( no->grupo, no->grupo + 1, no->num_grupos );
                  		     no->num_grupos++;
                                     *no->grupo = pto->no_grupo.grupo;
                                  }
   	                          grupo = t_grupos + grupo->grp_1;
                                  if ( grupo->status > 2  &&  grupo->status < 5 )
                                     while ( grupo->status < 5 )
                                           grupo = t_grupos + grupo->grp_1;
                            }

                      }

   }

   grupo = t_grupos + grup;
   pto = t_pontos + grupo->pto_fim;
   if ( !pto->no_grupo.no )
      pto->no_grupo.no = ++u_no;
   Introduz_Grupo_no_No( linha->grupo - 1, pto->no_grupo.no, linha );
   no = t_nos + pto->no_grupo.no;
   movmem( no->grupo, no->grupo + 1, no->num_grupos );
   no->num_grupos++;
   *no->grupo = linha->grupo - 1;

   return pto->no_grupo.no;


}                    /*  fim de Complementa_Nos_do_Grupo  */
