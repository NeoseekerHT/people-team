//ROTINAS 
module.exports = function saidas(res) {
    const fetch = require("node-fetch");
    const btoa = require('btoa');

    fetch("http://192.192.1.200:4000/b/SELECT idTraccar FROM equipe").then(
            function (resp) {
                resp.json().then(function (jsonQ) {
                    var vetor = [];
                    for (let l = 0; l < jsonQ.length; l++) {
                        if (jsonQ[l].idTraccar !== undefined) {
                            var entradas = require('./entradas');
                            vetor[l] = new entradas(jsonQ[l].idTraccar, res);
                        }
                    }
                    return jsonQ;
                });
            });

};