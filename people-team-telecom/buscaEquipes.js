module.exports = function sites(res) {
    const fetch = require("node-fetch");
    const btoa = require('btoa');

    var vetor = [];

    var url = "http://138.197.104.196/api/devices";
    fetch(url, {
        method: 'GET',
        headers: {
            'Accept': 'application/json',
            'Authorization': 'Basic ' + btoa('bgomes@people-team.com:People01'),
            'Content-Type': 'application/json',
            'Origin': ''
        }
    }).then(
            function (resp) {
                resp.json().then(function (json) {
                    for (let k = 0; k < json.length; k++) {
                        vetor[k] = {id: json[k].id, nome: json[k].name};
                    }

                    var entradas = require('./attEquipes');
                    new entradas(res, vetor);

                });
            });
};