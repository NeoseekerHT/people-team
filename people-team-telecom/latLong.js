//ROTINAS 
module.exports = function entradas(res) {
    const fetch = require("node-fetch");
    const btoa = require('btoa');
    var url = "http://138.197.104.196/api/geofences";
    var js = [];
    try {
        fetch(url, {
            method: 'GET',
            headers: {
                'Accept': 'application/json',
                'Authorization': 'Basic ' + btoa('bgomes@people-team.com:People01'),
                'Content-Type': 'application/json',
                'Origin': ''
            }
        }).then(
                function (resp) {
                    resp.json().then(function (jsonQ) {
                        let text = [];
                        let boo = false;
                        for (let l = 0; l < jsonQ.length; l++) {
                            text[l] = [];
                            text[l][0] = "";
                            text[l][1] = "";
                            let st = jsonQ[l].area;
                            let alc = -1;
                            for (let k = 0; k < st.length; k++) {
                                if (st[k] === "-") {
                                    alc++;
                                    boo = true;
                                } else if (st[k] === " " || st[k] === ",") {
                                    boo = false;
                                }

                                if (boo) {
                                    text[l][alc] += st[k];
                                }

                            }
                            js[l] = {idTraccar: jsonQ[l].id, latitude: text[l][0], longitude: text[l][1]};

                        }

                        //res.json(js);
                        var inserts = require('./importSites');
                        new inserts(js, res);

                    });
                });
    } catch (e) {
        console.log("Catch");
    }
};

